<?php 

class Currency
{
	public static function get_conversion($content,$request_type)
	{
		//conversion_date //YYYY-MM-DD
		Requirements::_verify_action($content,$request_type);
		$query_retrieval = AppDirectory::model('query-retrieval');
		$result = $query_retrieval->_get_conversion($content);

		if(count($result)>0){
			SharedResponse::query_response(17,$result);
		}
		else{
			SharedResponse::query_response(18);
		}
	}
	public static function get_currency_list()
	{
		$method['func-type'] = "get-list";
		$currencies_opt = new currencies_opt(array("method"=>$method));
	}
}
