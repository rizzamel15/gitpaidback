<?php 

class Token
{
	public static function get_token($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$token_opt 	= AppDirectory::model('token-opt');
		$response = $token_opt->_get(array('method'=>$content));

		if($response['bool']==false){
			SharedResponse::requirements_response_halt(7);
		}
		else{
			SharedResponse::query_response(6,$response['account_details']);
		}
	}
	public static function change_token($content)
	{
		$require_result = Requirements::_check_token($content);
		$check_data = AppDirectory::model('check-data');
		$consumer_log_id = $check_data->_check_credentials($require_result);
		$mobile_token = AppDirectory::model('mobile-token');
		$return_response = $mobile_token->_change_token($consumer_log_id);
		$array_response = Main_Response::_get_change_token_return($return_response);
		Response::json_output($array_response);
	}
}
