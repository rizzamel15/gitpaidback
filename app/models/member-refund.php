<?php
ob_start();
error_reporting(0);
session_start();

class Member_Refund extends Model
{
	public function refund($array_params)
	{
		$json_data = array(
			'descriptor' 		=> $array_params['descriptor'],
			'date-of-purchase'	=> $array_params['date_of_purchase'],
			'purchase'			=> $array_params['purchase'],
			'first6' 			=> $array_params['first6'],
			'last4' 			=> $array_params['last4'],
			'email-address' 	=> $array_params['email_address'],
			'prelogid'			=> $array_params['prelogid'],
			'purchase-name'		=> $array_params['purchase-name']
		);

		if(array_key_exists('reason', $array_params)){
			$json_data['reason'] = $array_params['reason'];
		}

		if(array_key_exists('tracking-number', $array_params)){
			$json_data['tracking-number'] = $array_params['tracking_number'];
		}

		foreach($json_data as $key=>$val){
			$json_data[$key] = urlencode($val); //PREVENTION OF ESCAPE like (+);
		} 

		$json_str = json_encode($json_data);
		// exit(print_r($json_str));

		$this->initiate_refund(array('content' => $json_str));
	}
	public function initiate_refund($array_params)
	{	
		// (
		//     [descriptor] => VARIANTSOLUTION
		//     [date-of-purchase] => 08%2F10%2F2015
		//     [purchase] => 5.00
		//     [first6] => 415422
		//     [last4] => 3171
		//     [email-address] => deylann%40gmail.com
		//     [prelogid] => 1368
		//     [purchase-name] => Mikee%26nbsp%3BBadang
		// )
		$_SESSION['api-type'] = "API";
		$_SESSION['ip-address'] = getip();
		$type = "api";
		// exit(print_r($array_params['content']));
		// $checking_class = new checking;
		
		$tcontent=json_decode(stripslashes($array_params['content']),true);

		$content_array = json_decode(urldecode(stripslashes($array_params['content'])),true);
		

		foreach($content_array as $key=>$val){
			$content_array[$key] = urldecode($val);
		}



		// $content_array = $checking_class->_sanitize(array('array-data'=>$content_array));
		$content_array['descriptor']=$tcontent['descriptor'];


        // $content_array['other-details'] = (isset($content_array['other-details'])) ? $content_array['other-details'] : '' ;

		// if(isset($content_array['prelogid']))
		// {
		$_SESSION['pre-logid'] = $content_array['prelogid'];
		// }

		//CHECK FORMATS
		Checking::_format(array('content-array'=>$content_array));	

		if(isset($array_params['id']))
		{
			$_SESSION['j-update'] = $array_params['id'];
		} 
		else{
			unset($_SESSION['j-update']);
		}
		
		if(!isset($_SESSION['j-update']))
		{
			unset($_SESSION['s-descriptor']);
			$d2desc = $content_array['descriptor'];

			// exit($d2desc);
			// $main_query = new main_query;
			// $query_string = "SELECT * FROM ".$db.".pre_descriptor WHERE descriptor ='$d2desc'";


			// $query_data = $main_query->_advance_key_array($main_query->_get($query_string));

			$query_data = $this->db->select("SELECT * FROM tempic4c.pre_descriptor WHERE descriptor = :descriptor", array(":descriptor" => $d2desc));

			
			if(count($query_data)>=1)
			{
				$_SESSION['desctype']='NEW';
			}
			else
			{
				
				// $main_query = new main_query;
				// $query_string15 = "SELECT ca.STATUS FROM (".$db.".descriptor des, ".$db.".merchantid mid, ".$db.".client_account ca) WHERE des.DESCRIPTOR_TEXT ='$d2desc' AND des.DESCRIPTOR_MID = mid.mid AND ca.CLIENT_ID = mid.clientID";
				
				// $query_data15 = $main_query->_advance_key_array($main_query->_get($query_string15));

				$query_data2 = $this->db->select("SELECT ca.STATUS FROM (tempic4c.descriptor des, tempic4c.merchantid mid, tempic4c.client_account ca) WHERE des.DESCRIPTOR_TEXT = :descriptor AND des.DESCRIPTOR_MID = mid.mid AND ca.CLIENT_ID = mid.clientID", array(":descriptor" => $d2desc));
				
				
				if($query_data2[0]['STATUS']=='INACTIVE')
				{
					$_SESSION['desctype']='NEW-PROC';
				}
				else
				{			

					$d2desc = $content_array['descriptor'];
					// exit($d2desc);
					// $main_query = new main_query;
					
					// $query_string = "
					// SELECT tmid.clientId,
					// tmid.mid FROM (".$db.".`descriptor`
					// tdes, ".$db.".`tbltriggeraccounts` tta) LEFT JOIN ".$db.".merchantid
					// tmid on tmid.mid = tdes.DESCRIPTOR_MID
					// AND tta.idClient = tmid.clientID WHERE
					// tdes.DESCRIPTOR_TEXT='$d2desc'  AND tmid.clientId
					// IS NOT NULL";


					$query_data = $this->db->select("SELECT tmid.clientId,tmid.mid FROM (tempic4c.`descriptor` tdes, tempic4c.`tbltriggeraccounts` tta) LEFT JOIN tempic4c.merchantid tmid on tmid.mid = tdes.DESCRIPTOR_MID AND tta.idClient = tmid.clientID WHERE tdes.DESCRIPTOR_TEXT = :descriptor  AND tmid.clientId IS NOT NULL", array(":descriptor" => $d2desc));

					// $query_data = $main_query->_advance_key_array($main_query->_get($query_string));
					
					////////////////////////////////////////////////
					///              WORKING ON THIS             ///
					////////////////////////////////////////////////
					// if(count($query_data)>=1)//real
					if(count($query_data)==0)//test
					{					
						exit('hello');
						$saving_class = AppDirectory::model('refund/saving');

						// $saving_class = new saving;
						$data = array(
							'query-data'=>$query_data,
							'content-array'=>$content_array,
							'merchant-data'=>$query_data[0]);
						return $saving_class->pending($data);		
					}
					else{
						exit('hey');
						unset($_SESSION['desctype']);
					}
				}
			}
			
			if((isset($array_params['content']))&&(isset($_SESSION['desctype'])))
			{
				$desctype=$_SESSION['desctype'];

				if($desctype=='NEW-PROC')
				{
					$main_query = new main_query;
								
					//--- Added to exempt descriptor (Symbols not added due to urldecode)
					$tcontent=json_decode(stripslashes($array_params['content']),true);
					//---
					$content_array = json_decode(urldecode(stripslashes($array_params['content'])),true);
					//--- Modified by Oli - 06/19/2015
					$content_array['descriptor']=$tcontent['descriptor'];
					//---
					$merchant_data = array();
					if(count($query_data)>=1)
					{
						$merchant_data = $query_data[0];
					}
					$saving_class = new saving;
								
					return $saving_class->new_proc
					(
					array (
					'query-data'=>$query_data,
					'content-array'=>$content_array,
					'merchant-data'=>$merchant_data
							)
					);			
				}
				else{}
			}
				
		}  
		
		if(isset($array_params['content']))
		{
			$_SESSION['s-descriptor']=$content_array['descriptor']; 
			$value = $api_class->_index(array('type'=>$type,'content-array'=>$content_array));
		}

		return $value;













		// if(( $response_data['responsecode']) == 4) // REFUND SUCCESS
		// {
		// 	SharedResponse::query_response(4,$response_data);
		// }
		// else{					//REFUND FAILED
		// 	SharedResponse::requirements_response_halt(5,$response_data);
		// }
		
	}
}