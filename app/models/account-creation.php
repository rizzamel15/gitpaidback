<?php
ob_start();
error_reporting(0);

class account_creation extends Model
{
	public function full_register ( $array_params ) 
    {
		$this->check_username( $array_params );
		$this->check_email( $array_params );
		
		$mm_insert = $this->insert_new_mm_account( $array_params );
		$nmm_insert = $this->insert_new_nmm_account( $array_params );
  
		if ( $nmm_insert['status'] == 'success' && $mm_insert['status'] == 'success' ) {
			
			$insert_other = $this->insert_other_email_address($mm_insert['pre_login_id'],$array_params);
			
			if ( $insert_other['status']=='success' ) {
				$this->verify_registration($array_params,$mm_insert['usrMD5']);
				$array_response = Main_Response::_query_response(3);
			}
		}
		else {
			
			$this->roll_back_changes( $array_params );
			$array_response = array('responsecode'=>-1,'responsestring'=>'Connection Timeout, Please try again!');
		}

        return $array_response;
	}
	
	public function check_username ( $array_params ) 
    {
        $query_data = $this->db->select("SELECT * FROM tempic4c.pre_login as pl JOIN tempic4c.floating_consumer as fc WHERE pl.USERNAME = :username  and pl.USERNAME=fc.FC_LOGINID", array(":username" => $array_params['username']));
		
		if(count($query_data) >= 1){
            SharedResponse::check_response_halt(1);
        }
	}
	
	public function check_email ( $array_params ) 
    {	
        $query_data = $this->db->select("SELECT * FROM tempic4c.pre_login as pl JOIN tempic4c.floating_consumer as fc WHERE pl.EMAIL_ADDRESS = :email_address and pl.EMAIL_ADDRESS = fc.FC_EMAIL_ADDRESS", array(":email_address" => $array_params['main_email_address']));
		
		if (count($query_data) >= 1){
            SharedResponse::check_response_halt(4);
		}
	}
	
	public function insert_new_nmm_account($array_params)
    {
        $values = array(
            "FC_FIRSTNAME"          =>  $array_params['first_name'],
            "FC_LASTNAME"           =>  $array_params['last_name'],
            "FC_LOGINID"            =>  $array_params['username'],
            "FC_PASSWORD"           =>  md5_new::_adv($array_params['password']),
            "FC_EMAIL_ADDRESS"      =>  $array_params['main_email_address'],
            "FC_PHONE_NUMBER"       =>  $array_params['phone_number'],
            "FC_DATE_ADDED"         =>  date('Y-m-d H:i:s'),
            "FC_DATE_MODIFIED"      =>  date('Y-m-d H:i:s')
        );

        if(array_key_exists('notification_promo', $array_params)){
            $values['FC_NOTIF_PROMO'] = substr($array_params['notification_promo'],0,1);
        }
        if(array_key_exists('notification_newmerch', $array_params)){
            $values['FC_NEWMERCH'] = substr($array_params['notification_newmerch'],0,1);
        }
        if(array_key_exists('notification_newsletter', $array_params)){
            $values['FC_NEWSLETTER'] = substr($array_params['notification_newsletter'],0,1);
        }

        $query_data = $this->db->insert("tempic4c.floating_consumer", $values);
        $consumer_id = $this->db->lastInsertId();

        // $to = "drizzamel15@gmail.com";
        // $subject = "My subject";
        // $headers = "From: webmaster@example.com";

        // mail($to,$subject,$query_string,$headers);
		
        if(isset($consumer_id)){
            $responseval = array('status'=>'success','consumer_id'=>$consumer_id);
        }
        else{
            $responseval = array('status'=>'error');
        }
        
        return $responseval;
    }
    
    public function insert_new_mm_account($array_params)
    {
        $timestamp = date('Ymdhis');
        $token = sha1($array_params['username'].$timestamp);
        $ip = $this->get_ip();
        $usrMD5 = md5(date('Y-m-d H:i:s'));

        $values = array(
            "FIRSTNAME"         =>  $array_params['first_name'],
            "LASTNAME"          =>  $array_params['last_name'],
            "USERNAME"          =>  $array_params['username'],
            "PASSWORD"          =>  md5_new::_adv($array_params['password']),
            "EMAIL_ADDRESS"     =>  $array_params['main_email_address'],
            "PHONE_NUMBER"      =>  $array_params['phone_number'],
            "DATE"              =>  date('Y-m-d H:i:s'),
            "IP"                =>  $ip,
            "MD5"               =>  $usrMD5,
            "VERIFIED"          =>  "0",
            "FATTEMPTS"         =>  "0",
            "mobile_token"      =>  $token);

        if(array_key_exists('notification_promo', $array_params)){
            $values['NOTIF_PROMO'] = substr($array_params['notification_promo'],0,1);
        }
        if(array_key_exists('notification_newmerch', $array_params)){
            $values['NOTIF_NEWMERCH'] = substr($array_params['notification_newmerch'],0,1);
        }
        if(array_key_exists('notification_newsletter', $array_params)){
            $values['NOTIF_NEWSLETTER'] = substr($array_params['notification_newsletter'],0,1);
        }

        $query_data = $this->db->insert("tempic4c.pre_login", $values);
        $pre_login_id = $this->db->lastInsertId();

        // $to = "drizzamel15@gmail.com";
        // $subject = "My subject";
        // $headers = "From: webmaster@example.com";

        // mail($to,$subject,$query_string,$headers);
						
        if(isset($pre_login_id))
        {
            $responseval = array('status'=>'success','pre_login_id'=>$pre_login_id,'usrMD5'=>$usrMD5);
        }else{
            $responseval = array('status'=>'error');
        }
        return $responseval;
    }
    
    public function insert_other_email_address($pre_login_id,$array_params)
    {
        $values = array("PRE_LOGIN_ID" => $pre_login_id);

        if(array_key_exists('notification_newsletter', $array_params)){
            $values['EMAILADDRESS'] = $array_params['other_email_list'];
        }

        $query_data = $this->db->insert("tempic4c.tblemailbank", $values);
        $consumer_id = $this->db->lastInsertId();

        if(isset($consumer_id )){
            $responseval = array('status'=>'success','consumer_id'=>$consumer_id);
        }
        else{
            $responseval = array('status'=>'error');
        }
        
        return $responseval;
    }
    
    public function roll_back_changes($array_params)
    {
        $query_data1 = $this->db->delete("tempic4c.pre_login", "USERNAME = '".$array_params['username']."'");
        $query_data2 = $this->db->delete("tempic4c.floating_consumer", "FC_LOGINID = '".$array_params['username']."'");
    }
    
    public function get_ip()
    {
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } 
        elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } 
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        return $ip;
    }
	
	public function verify_registration( $array_params , $usrMD5) 
    {                  
		$server = '//ican4consumers.com/ic4c-ajax/verify.php?type=consumer_reg&verify='.Main_Encdec::encrypturl($usrMD5);
		$_SESSION['verify']=1;

        $data = array(
            'server'                    => $server,
            'main-email-address'        => $array_params['main_email_address'],
            'first-name'                => $array_params['first_name'],
            'last-name'                 => $array_params['last_name'],
            'username'                  => $array_params['username'],
            'notification-newsletter'   => '',
            'notification-promo'        => '',
            'notification-newmerch'     => ''
        );

        if(array_key_exists('notification_newsletter', $array_params)){
            $data['notification-newsletter'] = $array_params['notification_newsletter'];
        }

        if(array_key_exists('notification_promo', $array_params)){
            $data['notification-promo'] = $array_params['notification_promo'];
        }

        if(array_key_exists('notification_newmerch', $array_params)){
            $data['notification-newmerch'] = $array_params['notification_newmerch'];
        }

        AppDirectory::view('emails/signup-account', $data);
    }
}

ob_end_flush();