<?php
ob_start();
error_reporting(0);

class Descriptor_Opt extends Model
{
	protected $descriptor = null;
	protected $like_optional = false;
	protected $limit = null;
	
	public function _index($array_params)
	{
		$method = $array_params['method'];
		$func_type  = $method['func-type'];		
		
		switch($func_type)
		{
			case "get-list-of-descriptor": //CAN BE USE ALSO FOR VERIFYING DESCRIPTOR
				$this->descriptor=$method['descriptor'];
				
				if(isset($method['like'])){$this->like_optional=true;}
				if(isset($method['limit'])){$this->limit=$method['limit'];}
				
				return $this->_get_list(); //START FETCHING OF DESCRIPTORS
			break;
		}
	}
	private function _get_list()
	{		
		$limit_str = "";
		
		// limit = 1,10 OPTIONAL
		if($this->limit!=null)
		{
			$limit_array = explode(",",$this->limit);
			
			if(count($limit_array)<=1){
				// limit-format-invalid
				SharedResponse::requirements_response_halt(6);
			}
			
			$limit_str = " LIMIT ".$this->limit;	
		}
		
		$where_str = " WHERE id.DESCRIPTOR_TEXT='".$this->descriptor."' ";
		
		//OPTIONAL
		if($this->like_optional==true){
			$where_str = " WHERE id.DESCRIPTOR_TEXT LIKE '%".$this->descriptor."%' ";
		}
		
		$order_str = " ORDER BY id.DESCRIPTOR_TEXT ASC ";
	
		return $this->db->select("SELECT DISTINCT(DESCRIPTOR_TEXT) FROM tempic4c.descriptor AS id ".$where_str.$order_str.$limit_str);
	}
}

ob_end_flush();