<?php

class nmm_refund
{
	protected $main_query = null;
	protected $url = 'https://www.ic4c.net/_api/api_non_member_merchant_for_test.php?';
	
	protected $json_data = array(
						   'merchant-info'			=> 	'',
						   'merchant-business'		=>	'',
						   'merchant-website'		=>	'',
						   'merchant-email'			=>	'',
						   'merchant-phone'			=>	'',
						   'consumer-firstname'		=>	'',
						   'consumer-lastname'		=>	'',
						   'consumer-email'			=>	'',
						   'consumer-phone'			=>	'',
						   'consumer-cardfirst'		=>	'',
						   'consumer-cardlast'		=>	'',
						   'purchase-date'			=>	'',
						   'purchase-amount'		=>	'',
						   'purchase-email'			=>	'',
						   'purchase-reason'		=>	'',
						   'purchase-product'		=>	'',
						   'consumer-loginid'		=>	'',
						   'consumer-password'		=>	'',
						   'receive-promotional'	=>	'',
						   'receive-newsletter'		=>	'',
						   'new-merchants'			=>	''
						   );
					 
	public function __construct($array_params)
	{
		$method = $array_params['method'];
		$method['receive-promotional'] = array_key_exists('receive-promotional', $method) ? $method['receive-promotional'] : '';
		$method['receive-newsletter'] = array_key_exists('receive-newsletter', $method) ? $method['receive-newsletter'] : '';
		$method['new-merchants'] = array_key_exists('new-merchants', $method) ? $method['new-merchants'] : '';
		
		$json_data =  array(
					   'merchant-info'		=>	$method['merchant-info'],
					   'merchant-business'	=>	$method['merchant-business'],
					   'merchant-website'	=>	$method['merchant-website'],
					   'merchant-email'		=>	$method['merchant-email'],
					   'merchant-phone'		=>	$method['merchant-phone'],
					   'consumer-firstname'	=>	$method['consumer-firstname'],
					   'consumer-lastname'	=>	$method['consumer-lastname'],
					   'consumer-email'		=>	$method['consumer-email'],
					   'consumer-phone'		=>	$method['consumer-phone'],
					   'consumer-cardfirst'	=>	$method['consumer-cardfirst'],
					   'consumer-cardlast'	=>	$method['consumer-cardlast'],
					   'purchase-date'		=>	$method['purchase-date'],
					   'purchase-amount'	=>	$method['purchase-amount'],
					   'purchase-email'		=>	$method['purchase-email'],
					   'purchase-reason'	=>	$method['purchase-reason'],
					   'purchase-product'	=>	$method['purchase-product'],
					   'consumer-loginid'	=>	$method['consumer-loginid'],
					   'consumer-password'	=>	$method['consumer-password'],
					   'receive-promotional'=>	$method['receive-promotional'],
					   'receive-newsletter'	=>	$method['receive-newsletter'],
					   'new-merchants'		=>	$method['new-merchants']
					   );
	
		$this->json_data = $json_data;
		$this->nmm_initiate_refund();
	}
	
	private function nmm_initiate_refund()
	{	
		$url = $this->url;
		$json_data = $this->json_data;
		
		$content = '';

		foreach($json_data as $key=>$val){
			$content .= $key.'='.$val.'&';
		}
		
		$json_str = json_encode($json_data);
		
		$post = 'apikey=!itr4ck3r92315@@&'.$content.'type=float-refund-add';

		$response = json_decode(Main_Curl::_curl($url,$post),true);
		$response_data = $response;
		
		if(( $response_data['responsecode']) == 1) // REFUND SUCCESS
		{
			SharedResponse::query_response(4,$response_data);
		}
		else
		{	//REFUND FAILED
			SharedResponse::requirements_response_halt(5,$response_data);
		}
	}
}