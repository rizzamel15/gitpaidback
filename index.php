<?php 
/**
 * Added
 * --------------------------------------------------*
 * 		gitpaidback	  : index.php 					 *
 * 		author        : rizzamel                 	 *
 * 		status        : on-going	 				 *
 *   	date started  : 04-13-2020		          	 *
 *   	date finished : ---------- 	     		 	 *
 *   	date modified : 05-05-2020 	     		 	 *
 * --------------------------------------------------*
 */
//request url: http://localhost/gitpaidback-modified/

date_default_timezone_set('America/New_York');
require 'app/core/app-directory.php';
AppDirectory::configuration();
AppDirectory::connection();
AppDirectory::library();
Bootstrap::_init();

