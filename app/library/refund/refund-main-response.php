<?php

class Refund_Main_Response
{
	public static function _final_response($type)
	{
		$array_response = array();
		
		switch($type)
		{
			//real success,unable to refund
			case "real success" : 
				$array_response = array(
					'responsecode'=>4,
					'responsestr'=>self::_long_resp(4),
					'errorstr'=>'');
			break;
			case "real success forwarding" : 
				$array_response = array(
					'responsecode'=>4,
					'responsestr'=>self::_long_resp(13),
					'errorstr'=>'');
			break;
			case "real success pending" : 
				$array_response = array(
					'responsecode'=>4,
					'responsestr'=>self::_long_resp(13),
					'errorstr'=>'');
			break;
			//real success pending = successful in requesting refund but got pending response
			 /*' [**** For testing only ****]@  '*/
			case "testing" : 
				$array_response = array(
					'responsecode'=>5,
					'responsestr'=>self::_long_resp(4),
					'errorstr'=>'');
			break;
			//ERRORS
			case "transaction not found" : 
				$array_response = array(
					'responsecode'=>2,
					'responsestr'=>'transaction not found',
					'errorstr'=>self::_long_resp(2));
			break;
			case "unable to refund" : 
				$array_response = array(
					'responsecode'=>1,
					'responsestr'=>'Unable to refund',
					'errorstr'=>self::_long_resp(9));
			break;
			case "unable to refund special" : 
				$array_response = array(
					'responsecode'=>1,
					'responsestr'=>'Unable to refund',
					'errorstr'=>self::_long_resp(20));
			break;
			case "date expire" : 
				$array_response = array(
					'responsecode'=>7,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(1));
			break;
			case "invalid descriptor":
				$array_response = array(
					'responsecode'=>-9,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(10));
			break;
			case "duplicate":
				$array_response = array(
					'responsecode'=>-9,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(15));
			break;
			case "missing" : 
				$array_response = array(
					'responsecode'=>-1,
					'responsestr'=>'Error found',
					'errorstr'=>'missing key');
			break;
			case "empty" :	
				$array_response = array(
					'responsecode'=>-2,
					'responsestr'=>'Error found',
					'errorstr'=>'empty fields found');
			break;
			case "blacklisted" : 
				$array_response = array(
					'responsecode'=>-3,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(8));
			break;
			case "invalid email" : 
				$array_response = array(
					'responsecode'=>-5,
					'responsestr'=>'Error found',
					'errorstr'=>'invalid email address format');
			break;
			case "invalid date" : 
				$array_response = array(
					'responsecode'=>-6,
					'responsestr'=>'Error found',
					'errorstr'=>'invalid date format');
			break;
			case "invalid card" : 
				$array_response = array(
					'responsecode'=>-7,
					'responsestr'=>'Error found',
					'errorstr'=>'invalid card');
			break;
			case "connection timeout" : 
				$array_response = array(
					'responsecode'=>-8,
					'responsestr'=>'Error found',
					'errorstr'=>'connection timeout');
			break;
			case "invalid amount": 
				$array_response = array(
					'responsecode'=>-10,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(11));
			break;
			case "greater amount": 
				$array_response = array(
					'responsecode'=>-11,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(12));
			break;
			case "date exceed":
				$array_response = array(
					'responsecode'=>-12,
					'responsestr'=>'Error found',
					'errorstr'=>self::_long_resp(14));
			break;
			case "api response empty":
				$array_response = [
					"responsecode"	=> -15,
					"responsestr"	=> "Error found",
					"errorstr"		=> "OOOPS!  looks like we encountered a connection error.  We want to make sure you receive your refund promptly so please call us at +1 855-660-3214, and we will be more than happy to take care of you.</br></br>Thank you for using iCAN 4 Consumers, the only completely independent Credit Adjustment Network for Consumers!"
				];
			break;		
			//END ERRORS
		}
		return $array_response;
	}
	public static function _long_resp($type)
	{
		$response = '';
		
		switch($type)
		{
			case 1:
				$response="We're unable to grant a Refund as the original transaction is more than [90 days past]. <br/><br/> If you have any questions or need assistance, please call us at [+1 855-660-3214]. <br/><br/> Thank you for using [iCAN 4 Consumers]!  - The only completely Independent Credit Adjustment Network for Consumers!";
			break;
			case 2:
				$response="The original transaction dated --[DATE]-- in the amount of --[AMOUNT]-- could not be located in our records for --[DESCRIPTOR]--. <br/><br/> Please confirm the date, amount, and billing descriptor from your credit card statement and submit your request again. <br/><br/> If you have any questions or need assistance, please call us at +1 855-660-3214. <br/><br/>Thank you for using iCAN 4 Consumers, the only completely independent Credit Adjustment Network for Consumers!";
			break;
			case 3:
				$response="Oh no! This merchant --[MERCHANT]-- is not a member of the iCAN network yet! <br/><br/> Please contact the merchant directly to resolve your dispute and remind them to enroll in the iCAN merchant network, the only independent network of merchants working for you, the consumer. Remember, the consumer never pays a fee for this service.";
			break;
			case 4:
				$response="A Refund for your original transaction dated --[DATE]-- for --[AMOUNT]-- has been successfully issued on your behalf by iCAN4consumers! <br/><br/>Your confirmation number is --[CONFIRMATION NUMBER]--, please print this notice for your records.<br/><br/>A credit will be posted to your credit card account ending in --[LAST4]-- as quickly as your Issuing <br/> bank elects to do so but generally takes no more than 5 to 7 days.  If you have any questions or need <br/> assistance, please call us at +1 855-660-3214.<br/><br/>Thank you for using iCAN 4 Consumers, the only completely independent Credit Adjustment Network for Consumers!";
			break;
			case 5:
				$response="Mid does not exist.";
			break;
			case 6:
				$response="Invalid username/password";
			break;
			case 7:
				$response="We're sorry , we still cannot locate your transaction. Please contact the merchant directly to request a refund.";
			break;
			case 8:
				$response="Please contact your merchant.";
			break;
			case 9:
				$response="We're sorry , we still cannot process your refund. Please contact the merchant directly to request a refund , Error Code : --[ERROR CODE]--";
			break;
			case 10:
				$response="Descriptor : --[DESCRIPTOR]-- cannot be located in our records.";
			break;	
			case 11:
				$response="Invalid amount format";
			break;	
			case 12:
				$response="Amount : --[AMOUNT]-- cannot provide a refund , limit reached.";
			break;
			case 13:
				$response="A Refund for your original transaction dated --[DATE]-- for --[AMOUNT]-- has been successfully requested on your behalf by iCAN4consumers! <br/><br/>Your confirmation number is --[CONFIRMATION NUMBER]--, please print this notice for your records.<br/><br/>If you have any questions or need <br/> assistance, please call us at +1 855-660-3214.<br/><br/>Thank you for using iCAN 4 Consumers, the only completely independent Credit Adjustment Network for Consumers!";$_SESSION['forwardsesh']=1;
			break;
			case 14:
				$response="We're unable to grant a Refund as it exceeds the current date. <br/><br/> If you have any questions or need assistance, please call us at [+1 855-660-3214]. <br/><br/> Thank you for using [iCAN 4 Consumers]!  - The only completely Independent Credit Adjustment Network for Consumers!";
			break;
			case 15:
				$response="A Refund request was already placed under those transaction details. <br/><br/> If you have any questions or need assistance, please call us at [+1 855-660-3214]. <br/><br/> Thank you for using [iCAN 4 Consumers]!  - The only completely Independent Credit Adjustment Network for Consumers!";
			break;
			case 20:
				$response="We’re sorry but we’re unable to process your request at this time.  Please contact the merchant for assistance in requesting your refund by calling --[ERROR CODE]--";
			break;
			default:
				$response="An error occured";
			break;
		}
		return $response;
	}
	public static function _get_code($type)
	{
		$response = 0;

		switch($type)
		{
			case self::_get(1):$response=1;break;//"Unable to process."
			case self::_get(2):$response=2;break;//"Transaction not found"
			case self::_get(3):$response=3;break;//"Merchant not a member"
			case self::_get(4):$response=4;break;//REFUND ISSUED
			case self::_get(5):$response=5;break;//"Mid does not exist."
			case self::_get(6):$response=6;break;//"Invalid username/password"
			case self::_get(7):$response=7;break;//"Unable to refund"
			case self::_get(8):$response=8;break;//BLACK LISTED
			default:$response=0;break;//"An error occured"
		}

		return $response;
	}
	public static function _get($type)
	{
		$response = '';

		switch($type)
		{
			case 1:$response="Unable to process.";break;
			case 2:$response="Transaction not found";break;
			case 3:$response="Merchant not a member";break;
			case 4:$response="REFUND ISSUED";break;
			case 5:$response="Mid does not exist.";break;
			case 6:$response="Invalid username/password";break;
			case 7:$response="Unable to refund";break;
			case 8:$response="Unable to continue.";break;
			default:$response="An error occured";break;
		}
		
		return $response;
	}
}