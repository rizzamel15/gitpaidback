<?php

class travertex
{
	public static function _getCredential($array_params,$frontpassword)
	{
		$consumer_account_str = "";
		
		if(isset($array_params['PRE_LOGIN_ID']))
		{
			$required_array = array(
				"request-type"=>"travertex-redirect",
				"apikey"=>"e9fe4338ea238030934975ca86c8a99d95d5e4aa",
				"consumer-id"=>$array_params['PRE_LOGIN_ID'],
				"firstname"=>$array_params['FIRSTNAME'],
				"lastname"=>$array_params['LASTNAME'],
				"email"=>$array_params['EMAIL_ADDRESS'],
				"username"=>$array_params['USERNAME'],
				"password"=>Main_Encdec::encrypturl($frontpassword),
				"return-only"=>true,
				"return-url-if-fail"=>urlencode("https://www.snazzytraveler.com/?"),
			);

			$consumer_account_str = http_build_query($required_array);	
		}

		return 'https://www.ican4consumers.com/travertex/?'.$consumer_account_str;
	}
}
