<?php

class Main_Response
{
	public static function _get_change_token_return($return_token)
	{
		$array_response = array('response_code'=>1,'token'=>$return_token,'Status'=>'Success');
		return $array_response;
	}
	public static function _query_response($array_params,$extra_params = null)
	{
		$response_code = $array_params;
		
		switch($response_code)
		{
			case 1:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Email list successfully updated.');
			break;
			case 2:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Password successfully updated.');
			break;
			case 3:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Successfully registered. Sent verification link to main email.');
			break;
			case 4:
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Refund successful','Response-Details'=>$response_data);
			break;
			case 4.5:
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Currency listed','Response-Details'=>$response_data);
			break;
			case 5:
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Descriptor fetch success','Response-Details'=>$response_data);
			break;
			case 6:
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Token fetch success','Response-Details'=>$response_data);
			break;
			case 7:
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Fetch history success','Response-Details'=>$response_data);
			break;
			case 8:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Account name successfully updated.');
			break;
			case 9:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Username successfully updated.');
			break;
			case 10:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Main Email Address successfully updated.');
			break;
			case 11:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Email Address successfully deleted.');
			break;
			case 12:
				$array_response = array('response_code'=>-1,'Status'=>'Problem','Response'=>'Incorrect username or password.');
			break;
			case 14:
				$array_response = array('response_code'=>301,'Status'=>'Success','Response'=>'Tracking number required.');
			break;
			case 15:
				$array_response = array('response_code'=>300,'Status'=>'Success','Response'=>'NO tracking number required.');
			break;
			case 16:
				$array_response = array('response_code'=>302,'Status'=>'Problem','Response'=>'Invalid descriptor.');
			break;
			case 17:
				$response_data = $extra_params;
				$array_response = array('response_code'=>500,'Status'=>'Success','Response'=>$response_data);
			break;
			case 18:
				$array_response = array('response_code'=>501,'Status'=>'Problem','Response'=>'No data for conversion date');
			break;
			case 19:
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Login success','Response-Details'=>$response_data);
			break;
			case "location-response-ok":
				$response_data = $extra_params;
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Location successfully fetched','Response-Details'=>$response_data);
			break;
		}

		return $array_response;
	}
	
	public static function check_response_halt($array_params,$extra_params = null) // Handles abrupt errors
	{
		$response_code = $array_params;
		
		switch($response_code)
		{
			case 0:
				$array_response = array('response_code'=>200,'Status'=>'Problem','Response'=>'Credential check: Invalid credentials.');
			break;
			case 1:
				$array_response = array('response_code'=>201,'Status'=>'Problem','Response'=>'Username check: Already exists.');
			break;
			case 2:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Username check: Username available.');
			break;
			case 3:
				$array_response = array('response_code'=>500,'Status'=>'Problem','Response'=>'Incorrect parameter detected.');
			break;
			case 4:
				$array_response = array('response_code'=>201,'Status'=>'Problem','Response'=>'Email Address check: Already exists.');
			break;
			case 5:
				$array_response = array('response_code'=>1,'Status'=>'Success','Response'=>'Email Address check: Email Address available.');
			break;
			case 6:
				$array_response = array('responsecode'=>-1,'Status'=>'Problem','responsestring'=>'Username check: Username does not exist.');
			break;
			case 7:
				$array_response = array('responsecode'=>-1,'Status'=>'Problem','responsestring'=>'Email Address check: Email Address does not exist.');
			break;
			case 8:
				$array_response = array('responsecode'=>-1,'Status'=>'Problem','responsestring'=>'There is no active account registered under that email address.');
			break;
			case 9:
				$array_response = array('responsecode'=>-1,'Status'=>'Problem','responsestring'=>'That username does not exist or is not yet verified.');
			break;
		}
		
		return $array_response;
	}
	
	public static function requirements_response_halt($array_params,$extra_params = null) // Handles abrupt errors
	{
		$response_code = $array_params;
		
		switch($response_code)
		{
			case 0:
				$array_response = array('response_code'=>100,'Status'=>'Problem','Response'=>'Error: Invalid JSON format.');
			break;
			case 1:
				$array_response = array('response_code'=>101,'Status'=>'Problem','Response'=>'Error: Missing/Null Value Parameters.');
			break;
			case 2:
				$array_response = array('response_code'=>102,'Status'=>'Problem','Response'=>'Error: Invalid Token.');
			break;
			case 3:
				$array_response = array('response_code'=>103,'Status'=>'Problem','Response'=>'Error: Missing Token.');
			break;
			case 4:
				$array_response = array('response_code'=>104,'Status'=>'Problem','Response'=>'Error: Undefined Action.');
			break;
			case 5: //REFUND FAILED
				$response_data = $extra_params;
				$array_response = array('response_code'=>104,'Status'=>'Problem','Response'=>'Refund failed','Response-Details'=>$response_data);
			break;
			case 6:
				$array_response = array('response_code'=>104,'Status'=>'Problem','Response'=>'Invalid format.');
			break;
			case 7:
				$array_response = array('response_code'=>104,'Status'=>'Problem','Response'=>'Invalid username or password');
			break;
			case 8:
				$array_response = array('response_code'=>104,'Status'=>'Problem','Response'=>'Account not yet verified');
			break;
			case 9:
				$array_response = array('response_code'=>104,'Status'=>'Problem','Response'=>'Content parameter `search_criteria` invalid'); 
			break;
			case 10:
				$array_response = array('response_code'=>103,'Status'=>'Problem','Response'=>'Error: Missing API Key.'); 
			break;
			case 11:
				$array_response = array('response_code'=>103,'Status'=>'Problem','Response'=>'Error: Undefined Action Type.');
			break;
			case 12:
				$array_response = array('response_code'=>103,'Status'=>'Problem','Response'=>'Error: Undefined Recovery Type.');
			break;
		}
		return $array_response;
	}
} 