<?php
ob_start();
error_reporting(0);

class Account_Recovery extends Model
{	
	public function forgot_username($array_params)
	{
		$email_address = $array_params['email_address'];

		return $this->db->select("SELECT USERNAME,EMAIL_ADDRESS FROM tempic4c.pre_login AS ipl WHERE ipl.EMAIL_ADDRESS= :email_address and ipl.VERIFIED='1' ", array(":email_address" => $email_address));
	}
	public function forgot_password($array_params)
	{
		$username = $array_params['username'];
		$md5link = md5(date("YmdHis"));

		$query_data = $this->db->select("SELECT PRE_LOGIN_ID,EMAIL_ADDRESS FROM tempic4c.pre_login AS ipl WHERE ipl.USERNAME = :username and ipl.VERIFIED='1'", array(":username" => $username));

		if(count($query_data)>=1)
        {
        	$preid = $query_data[0]['PRE_LOGIN_ID'];
        	$post_data = array('CPASS' => $md5link);
			$this->db->update("tempic4c.pre_login", $post_data, "`PRE_LOGIN_ID` = {$preid}");
			$array_data = array('email-address'=>$query_data[0]['EMAIL_ADDRESS'], 'md5link'=>$md5link);
			AppDirectory::view('emails/forgot-password', $array_data);
		}
		else{
			SharedResponse::check_response_halt(9);
		}
	}
}

ob_end_flush();