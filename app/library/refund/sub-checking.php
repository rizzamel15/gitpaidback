<?php

class Sub_Checking 
{
	public static function _is_date($date)
	{
		$bool = false;
		$date_array = explode('/',$date);
		if(count($date_array)==3)
		{
			$month = intval($date_array[0]);
			$day = intval($date_array[1]);
			$year = intval($date_array[2]);
			
			$yearlen = strlen($year);
			
			if($month<=12 && $day<=31 && $yearlen==4){$bool=true;}
		}
		return $bool;
	}
	public static function _is_email($email)
	{
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$bool = false;
		}
		else{
			$bool=true;
		}
		
		return $bool;
	}
	
	var $curr_card_type = array(
		array('VISA',4),
		array('MC',5)
	);
	
	public function _is_valid_card($array_params)
	{
		$bool = false;
		$content_array = $array_params['content-array'];
		$first6 = ($content_array['first6']);
		$last4 = ($content_array['last4']);
		$cardtype = $content_array['cardtype'];

		$curr_card_type = $this->curr_card_type;
		
		if(strlen($first6)==6 && strlen($last4)==4)
		{
			$in_type = false;
			$in_prefix = false;
			for($x=0;$x<count($curr_card_type);$x++)
			{
				if($cardtype==$curr_card_type[$x][0])
					{
					$in_type=true;
					$prefix = intval(substr($first6,0,1));
					if($prefix==$curr_card_type[$x][1]){$in_prefix=true;}
					}
			}
			if($in_type==true && $in_prefix==true){$bool = true;}
		}
		return $bool;
	}
	
}