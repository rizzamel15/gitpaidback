<?php
ob_start();
error_reporting(0);

class nmm_class extends Model
{
	// function check_username_nmm($array_params)
	// {
	// 	$query_string = "SELECT * FROM ic4c.FLOATING_MERCHANT WHERE FC_LOGINID = '".$array_params['username']."'";
	// 	$query_data = $main_query->_advance_key_array($main_query->_get($query_string));
		
	// 	$username_availability = 0;
		
	// 	if(count($query_data)<1)
	// 	{
	// 		$username_availability = 1;
	// 	}
		
	// 	return $username_availability;
	// }

	public function nmm_verify_token($array_params)
	{
		$nmm_consumerid = 0;

		$query_data = $this->db->select("SELECT `fc`.FC_ID FROM tempic4c.floating_consumer AS `fc` LEFT OUTER JOIN tempic4c.pre_login AS `prelog` on `fc`.FC_LOGINID = `prelog`.USERNAME WHERE `prelog`.mobile_token = :mobile_token", array(":mobile_token" => $array_params));

		if(count($query_data)<1){
			SharedResponse::requirements_response_halt(2);
		}

		$nmm_consumerid = $query_data[0]['FC_ID'];
		return $nmm_consumerid;
	}

	public function get_consumer_info($array_params)
	{	
		$query_data = $this->db->select("SELECT FC_LOGINID,FC_PASSWORD,FC_EMAIL_ADDRESS,FC_PHONE_NUMBER FROM tempic4c.floating_consumer WHERE FC_ID = :array_params", array(":array_params" => $array_params));
		
		if(count($query_data)==0){
			SharedResponse::requirements_response_halt(2);
		}

		return array('consumer-loginid'=>$query_data[0]['FC_LOGINID'],'consumer-password'=>$query_data[0]['FC_PASSWORD'],'consumer-email'=>$query_data[0]['FC_EMAIL_ADDRESS'],'consumer-phone'=>$query_data[0]['FC_PHONE_NUMBER']);
	}
	
	// public function insert_new_nmm_account($array_params)
	// {
	// 	$main_query = new main_query;
	// 	$_date = date("Y-m-d H:i:s");
	// 	$query_string = "INSERT INTO tempic4c.FLOATING_CONSUMER 
	// 					(
	// 					FC_FIRSTNAME,
	// 					FC_LASTNAME,
	// 					FC_LOGINID,
	// 					FC_PASSWORD,
	// 					FC_EMAIL_ADDRESS,
	// 					FC_DATE_ADDED,
	// 					FC_DATE_MODIFIED,
	// 					FC_NOTIF_PROMO,
	// 					FC_NEWSLETTER,
	// 					FC_NEWMERCH)
	// 					VALUES
	// 					(
	// 					'".$array_params['first_name']."',
	// 					'".$array_params['last_name']."',
	// 					'".$array_params['username']."',
	// 					'".$array_params['password']."',
	// 					'".$array_params['main_email_address']."',
	// 					'".$_date."',
	// 					'".$_date."',
	// 					'".substr($array_params['notification_promo'],0,1)."',
	// 					'".substr($array_params['notification_newmerch'],0,1)."',
	// 					'".substr($array_params['notification_newsletter'],0,1)."')";
		
	// 	$responseval = array('status'=>'error');

	// 	// if($this->check_username_nmm($array_params)){
	// 		$consumer_id=$main_query->_set($query_string,1);
	// 	// }else{$responseval = array('status'=>'exist');}
		
	// 	if(is_numeric($consumer_id))
	// 	{
	// 		$responseval = array('status'=>'success','consumer_id'=>$consumer_id);
	// 	}
		
	// 	return $responseval;
	// }
	
	// function roll_back_changes($array_params)
	// {
	// 	$query_string = "DELETE FROM ic4c.FLOATING_CONSUMER WHERE FC_LOGINID = '".$array_params['username']."'; ";
	// 	$this->main_query->_set($query_string); 
	// }

	// public function _get_refund_requests($array_params)
	// {

	// 	$main_curl = new main_curl;
	// 	$main_response = new Main_Response;

	// 	$content_array=$array_params['search_criteria'];

		
	// 				//$require_class->_check_json(trim($content['search_criteria']));
	// 				//$search_criteria = json_decode($content['search_criteria'],true);
	// 				//REFERENCE,DESCRIPTOR,AMOUNT,DATE_OF_PURCHASE,EMAIL_ADDRESS

	// 		/**************************************************************/
	// 		/**************************************************************/	
	// 		//REFERENCE		
	// 				$allowed_list = array('descriptor','status','reference','amount','date_of_purchase','email_address');
	// 				$is_allowed = true;
	// 				$search_criteria_keys = array_keys($content_array);

	// 				foreach ($content_array as $key => $value) 
	// 				{
	// 					if(!in_array($key, $allowed_list)){$is_allowed = false;}
	// 				}

	// 			if($is_allowed==false){$main_response->requirements_response_halt(9);}

	// 		/**************************************************************/
	// 		/**************************************************************/	
				


		
	// 	$fc_id=$array_params['logid'];

	// 	$value='';

	// 	$_SESSION['limit'] = $array_params['sort-count'];
	// 	$_SESSION['page-start'] = $array_params['page-start'];

	// 	$page_start=$_SESSION['page-start'] * $_SESSION['limit'];
		
	// 	$limit_str=$page_start.','.$_SESSION['limit'];


	// 	$untildate='';

	// 	$search_str = '';

	// 	$search_type = '';

	// 	if(isset($content_array['status'])){$search_type='status';}
		
	// 	if(isset($content_array['reference'])){$search_type='reference';}

	// 	if(isset($content_array['descriptor'])){$search_type='descriptor';}

	// 	if(isset($content_array['amount'])){$search_type='amount';}

	// 	if(isset($content_array['date_of_purchase'])){$search_type='date_of_purchase';}		

	// 	if(isset($content_array['email_address'])){$search_type='email_address';}		

	// 	//'status','reference','descriptor','amount','date_of_purchase','email_address'
	// 	switch($search_type)
	// 	{
	// 		case "status":

	// 		$operator='(LIKE)';
	// 		$status='NRQ';
	// 		$key_status=$content_array['status'];

	// 			$status_type =array('ALL'=>array('(LIKE)','NRQ','(LIKE)'),
	// 							'REVIEW'=>array('(LIKE)','NRQ','(LESSER)(EQUAL)'),
	// 							'PROCESSED'=>array('(NOT-EQUAL)','NRQ','(LESSER)(EQUAL)'),
	// 							'APP'=>array('(LIKE)','APP','(LESSER)(EQUAL)'),
	// 							'DEC'=>array('(LIKE)','DEC','(LESSER)(EQUAL)'));

	// 			$operator=$status_type[$key_status][0];
	// 			$status=$status_type[$key_status][1];
	// 			$operandate=$status_type[$key_status][2];
	// 			$searchdate=date("Y-m-d");
	// 			if($key_status=='ALL'){$searchdate='(PERCENT)';}
	// 			$untildate='(AND)DATE(FU_UNTIL_DATE)'.$operandate.'(QUOTE)'.$searchdate.'(QUOTE)';

	// 			$search_str='FU_REFUND_STATUS (EQUAL) (QUOTE)'.$status.'(QUOTE)';
	// 			//$untildate='(AND)FU_UNTIL_DATE(LESSER)(QUOTE)'.date("Y-m-d").'(QUOTE)';
	// 		break;
	// 		//=========================================================================z
	// 		//=========================================================================z
	// 		case "reference":
	// 		//239
	// 			$search_str='FU_ID(EQUAL)'.$content_array['reference'];
	// 		break;

	// 		//=========================================================================z
	// 		//=========================================================================z
	// 		case "amount":
	// 			$search_str='FU_AMOUNT(EQUAL)'.$content_array['amount'];
	// 		break;
	// 		//=========================================================================z
	// 		//=========================================================================z
	// 		case "date_of_purchase":
	// 			$content_array['date_of_purchase'] = date("Y-m-d",strtotime($content_array['date_of_purchase']));

	// 			$search_str='FU_DATE_OF_PURCHASE(LIKE)(QUOTE)(PERCENT)'.$content_array['date_of_purchase'].'(PERCENT)(QUOTE)';
	// 		break;
	// 		//=========================================================================z
	// 		//=========================================================================z
	// 		case "email_address":
	// 			$search_str='FU_EMAIL_USED_FOR_PURCHASE(LIKE)(QUOTE)(PERCENT)'.$content_array['email_address'].'(PERCENT)(QUOTE)';
	// 		break;
	// 		case "descriptor":
	// 			$search_str='FMI_INFO(LIKE)(QUOTE)(PERCENT)'.$content_array['descriptor'].'(PERCENT)(QUOTE)';
	// 		break;
	// 	}

	// 	//exit($search_str);

	// 	$api_key = '!itr4ck3r92315@@';
	// 	$type = 'float-refund-view';
	// 	//$where = $search_str.'(AND)FC_ID(EQUAL)(QUOTE)'.$fc_id.'(QUOTE)'.$untildate;
	// 	$where = $search_str.'(AND)FC_ID(EQUAL)'.$fc_id.''.$untildate;
	// 	$limit = $limit_str;
	// 	$orderby = 'FU_UNTIL_DATE';
	// 	$order_type = 'ASC';
	// 	// $url = "http://www.ic4c.net/_api/api_non_member_merchant.php";
	// 	$url = "http://localhost/project-list/test-tracker3/_api/api_non_member_merchant.php";
	// 	$trycount=0;
 //        //mail('reign13120@gmail.com','Debugger',$url.'?type='.$type.'&apikey='.$api_key.'&where='.$where.'& ='.$limit.'&order-by='.$orderby.'&order-type='.$order_type,'From:donotreply@debugger.com');
			
	// 	do
	// 	{
	// 		$trycount++;
	// 		$curl_data = json_decode(stripcslashes($main_curl->_curl($url,'type='.$type.'&apikey='.$api_key.'&where='.$where.'&limit='.$limit.'&order-by='.$orderby.'&order-type='.$order_type)),true);
	// 		//$curl_data['response-latest-query-string'] = rawurldecode($curl_data['response-latest-query-string']);
	// 		unset($curl_data['response-latest-query-string']);
	// 		//exit("<pre>".print_r($curl_data,true)."</pre>");

	// 		if(count($curl_data['responsedetails'])>=1)
	// 		{
	// 			// $curl_data['responsedetailsextra']['statustype']=$key_status;
	// 			$value=$curl_data;
	// 		}
	// 		else
	// 		{
	// 			$value = array('responsecode'=>-1,'responsestring'=>'No data found.','responsedetails'=>Null);
	// 		}
	// 		if ($trycount>=3)
	// 		{
	// 			$value = array('responsecode'=>-1,'responsestring'=>'Failed to Retrieve Records.','responsedetails'=>Null);
	// 			break;
	// 		}
			
	// 	}
	// 	while(!isset($curl_data['responsecode']));

	// 	return $value;
	// }

	public function get_refund_list($array_params) 
	{	
		$search_by = $this->format_search_by_nmm( $array_params['search_by'] );
		$sort_by = $this->format_sort_nmm( $array_params['sort-by'] );
		$FC_ID = $array_params['logid'];

		// $url = "https://www.ic4c.net/_api/api_non_member_merchant_test.php";//change this.
		$url = '';
		$postfields = 'type=float-refund-view&apikey=!iw0rdpr3ss92315@@&where=FU_FC_ID(EQUAL)(QUOTE)'.$FC_ID.'(QUOTE)';

		if ($array_params['keyword'] != '' )
		{
			if ($search_by == 'FU_DATE_REQUESTED' && $array_params['keyword'] != '') {
				$postfields .= '(AND)'.$search_by.'(LIKE)(QUOTE)(PERCENT)'.$array_params['keyword'].'(PERCENT)(QUOTE)';
			}
			else{
				$postfields .= '(AND)'.$search_by.'(EQUAL)(QUOTE)'.$array_params['keyword'].'(QUOTE)';
			}
		}
		
		$postfields .= '&order-by='.$sort_by.'&order-type='.$array_params['sort-type'];
		
		$to = "drizzamel15@gmail.com";
		$subject = "post query";
		$headers = "From: webmaster@example.com";

		mail($to,$subject,$postfields,$headers);//icomment muna may error ee.

		// AppDirectory::file('app/library/emails/index');
  //   	Email::content($to,$subject,$postfields,$headers);
		
		$response = Main_Curl::_curl($url,$postfields);
		$response = $this->stripslashes_deep( $response );
		
		exit($response);
	}
	
	public function stripslashes_deep( $value ) {

		return is_array($value) ? array_map('stripslashes_deep', $value) :stripslashes($value);
	}
	public function format_search_by_nmm($array_params)
	{
		switch($array_params)
		{
			case "confirmation_number":
				$search_by = 'FU_ID';
			break;
			
			case "descriptor":
				$search_by = 'FMI_INFO';
			break;
			
			case "amount":
				$search_by = 'FU_AMOUNT';
			break;
			
			case "date_requested":
				$search_by = 'FU_DATE_REQUESTED';
			break;
			
			case "email_address":
				$search_by = 'FU_EMAIL_USED_FOR_PURCHASE';
			break;
		}
		
		return $search_by;
	}
	
	public function format_sort_nmm($array_params)
	{
		switch($array_params)
		{
			case "sort_amount":
				$sort_by = 'FU_AMOUNT';
			break;
			
			case "sort_date_requested":
				$sort_by = 'FU_DATE_REQUESTED';
			break;
			
			case "sort_email_address":
				$sort_by = 'FU_EMAIL_USED_FOR_PURCHASE';
			break;
		}
		
		return $sort_by;
	}
}

ob_end_flush();