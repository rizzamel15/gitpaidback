<?php 

class Refund
{
	public static function _refund($content,$request_type,$token)//add token here...
	{
		$content['token'] = $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$query_retrieval = AppDirectory::model('query-retrieval');
		$purchase_name = $query_retrieval->_get_account_name($consumer_log_id);
		$content['prelogid'] = $consumer_log_id;
		$content['purchase-name'] = $purchase_name;
		
		$member_refund = AppDirectory::model('member-refund');
		$query_data = $member_refund->refund($content);


		// $array_response = new refund_external(array('method'=> $content));
		// Response::json_output($array_response);
	}
	public static function get_history($content,$request_type,$token)
	{
		Requirements::_verify_action(array('token'=>$token),$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$query_condition = History_Opt::_query_generator($content);
		$query_retrieval = AppDirectory::model('query-retrieval');
		$query_data = $query_retrieval->_get_history($consumer_log_id,$query_condition);
		SharedResponse::query_response(7,$query_data);
	}
	public static function nmm_refund($content,$request_type,$token)
	{
		Requirements::_verify_action($content,$request_type);

		if($token != '')
		{
			$nonmember_class = AppDirectory::model('nmm-class');
			$fc_id = $nonmember_class->nmm_verify_token($token);

			$content['logid'] = $fc_id;
			$fc_info = $nonmember_class->get_consumer_info($fc_id);
			$content['consumer-loginid'] = '';
			$content['consumer-password'] = '';
			$content['consumer-email'] = $fc_info['consumer-email'];
			$content['consumer-phone'] = $fc_info['consumer-phone'];
		}
		
		$nmm_refund = new nmm_refund(array('method'=>$content));
	}
	public static function nmm_get_history($token,$request_type,$content)
	{
		$required_array = array();
		$required_array['token'] = $token;
		Requirements::_verify_action($required_array,$request_type);
		$nonmember_class = AppDirectory::model('nmm-class');
		$fc_id = $nonmember_class->nmm_verify_token($token);
		$content['logid'] = $fc_id;
		$array_response = $nonmember_class->get_refund_list($content);
		Response::json_output($array_response);
	}
}
