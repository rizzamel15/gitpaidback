<?php
ob_start();
error_reporting(0);

class Check_Data extends Model
{
	public function _check_username_availability($username,$type=null)
	{
		$query_data = $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE USERNAME = :username", array(":username" => $username));

		return array("type" => $type, "query-data" => $query_data);
	}
	public function _check_email_availability($email_address,$type=null)
	{
		$query_data = $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE EMAIL_ADDRESS = :email_address", array(":email_address" => $email_address));

		return array("type" => $type, "query-data" => $query_data);
	}
	public function _check_password($array_params)
	{
		$data = array(
			":password" 	=> md5_new::_adv($array_params['old_password']), 
			":mobile_token" => $array_params['token']);
	
		return $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE password = :password AND mobile_token = :mobile_token", $data);
	}
	public function _check_account_name($array_params)
	{
		$data = array(
			":old_firstname" 	=> $array_params['old_firstname'], 
			":old_lastname" 	=> $array_params['old_lastname'], 
			":mobile_token" 	=> $array_params['token']);

		return $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE FIRSTNAME = :old_firstname AND LASTNAME = :old_lastname AND mobile_token = :mobile_token", $data);
	}
	public function _check_username($array_params)
	{
		$data = array(
			":old_username" 	=> $array_params['old_username'],
			":mobile_token" 	=> $array_params['token']);

		return $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE USERNAME = :old_username AND mobile_token = :mobile_token", $data);
	}
	public function _main_email_address($array_params)
	{
		$data = array(
			":email_address" 	=> $array_params['old_main_email_address'],
			":mobile_token" 	=> $array_params['token']);

		return $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE EMAIL_ADDRESS = :email_address AND mobile_token = :mobile_token", $data);
	}
	public function _check_other_email($array_params)
	{
		$data = array(
			":email_id" => $array_params['email_id'],
			":email_address" => $array_params['email_address']
		);

		return $this->db->select("SELECT EMAIL_ID FROM tempic4c.tblemailbank WHERE EMAIL_ID = :email_id AND EMAILADDRESS = :email_address", $data);
	}
	
	public function _check_credentials($array_params)
	{
		$return_data = 0;

		$data = array(
			":username" 	=> $array_params['consumer_user'], 
			":password" 	=>  md5_new::_adv($array_params['consumer_pass']));

		$query_data = $this->db->select("SELECT PRE_LOGIN_ID, USERNAME, PASSWORD FROM tempic4c.pre_login WHERE USERNAME = :username AND PASSWORD = :password AND VERIFIED='1' limit 1", $data);
	
		if(count($query_data)==0){
			SharedResponse::check_response_halt(0);
		} 
		
		$return_data = $query_data[0]['PRE_LOGIN_ID'];
		return $return_data; 
	}
} 

ob_end_flush();