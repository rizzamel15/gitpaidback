<?php 

class Descriptor
{
	public static function get_descriptor($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$method = $content;
		$method['func-type'] = 'get-list-of-descriptor';
		$descriptor = AppDirectory::model('descriptor-opt');
		$query_data = $descriptor->_index(array('method'=>$method));

		$descriptor_list = array();
		
		for($x = 0; $x < count($query_data); $x++){
			array_push($descriptor_list,$query_data[$x]['DESCRIPTOR_TEXT']);
		}

		SharedResponse::query_response(5,$descriptor_list);
	}
	public static function tracking_descriptor($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$query_retrieval = AppDirectory::model('query-retrieval');
		$result = $query_retrieval->_get_descriptor_type($content);

		if(count($result)>0)
		{
			$query_decoded = json_decode($result[0]['extra_field'],true);

			if($query_decoded != '' && array_key_exists('tracking-number',$query_decoded)){
				SharedResponse::query_response(14);
			}
			else{SharedResponse::query_response(15);}
		}
		else{SharedResponse::query_response(16);}
	}
}
