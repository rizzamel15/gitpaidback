<?php

class Refund_Email
{
	public static function _success_error($array_params)
	{
		//'code'=>$error_code,'content-array'=>$content_array
		$code = $array_params['code'];
		$content_array = $array_params['content-array'];

		$img = 'http://www.ic4c.net/staging/_images/ican_email_png.png';
		if (isset($content_array['ycc_request'])) {
			$img = 'https://www.yourcreditcardcompany.com/assets/img/title.png" style="width: 500px;';
		}
		
		$subject = 'iCAN4Consumers';
		$from = 'confirmation-do-not-reply@ic4c.net';
		$to= $content_array['email-address'];
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$from = $headers.'From: '.$from;
		
		$class = 'error';
		
		$tr = '
				<tr>
				<td colspan="2" class="subtitle" style="color:red;font-style:italic;text-align:right;padding:5px;">Unable to refund.</td>
				</tr>
				<tr>';
		// $tr .= '
		// 		<td class="subtitle" style="color:gray;font-style:italic;">Error Code: </td>
		// 		<td class="subdata '.$class.'" style="font-weight:bold;color:red;">'.$code.'</td>
		// 	  	</tr>';
		
		$type = $array_params['type'];
		
		if($type=='success')
		{
			
			$class='success';
			$replyline= " refunded";
			if(isset($_SESSION['forwardsesh']))
			{
				if($_SESSION['forwardsesh']==1)
				{
					$replyline=" submitted";
					unset($_SESSION['forwardsesh']);
				}
			}
				$tr='<tr><td colspan="2" class="subtitle" style="color:green;font-style:italic;text-align:left;padding:5px;font-size:14px">Refund request successfully'.$replyline.'.</td><td></td></tr>';
				$tr.='<tr><td></td><td></td></tr>';
				$tr.='<tr><td></td><td></td></tr>';
				$tr.='<tr><td></td><td></td></tr>';
				// $tr.='<tr>
				// <td class="subtitle" style="color:gray;font-style:italic;">Confirmation Code: </td>
				// <td class="subdata '.$class.'" style="font-weight:bold;color:green;">'.$code.'</td>
				// </tr>';
		
		}
		
		$message = '<table class="main-info" style="border-collapse: collapse;">';	
		$message.='<tr><td></td><td></td></tr>';
		$message.='<tr><td><img src="'.$img.'"></td><td></td></tr>';
		$message.='<tr><td></td><td></td></tr>';
		$message.='<tr><td></td><td></td></tr>';
		$message.='<tr><td></td><td></td></tr>';
		$message.=$tr; 
		$message.='</table>';
		$message.='<table class="main-info" style="border-collapse: collapse;">';
		if ($class=='success') {
			$message.='<tr>
			<td class="subtitle" style="color:gray;font-style:italic;">Confirmation Code: </td>
			<td class="subdata '.$class.'" style="font-weight:bold;color:green;">'.$code.'</td>
			</tr>';
		}else{
			$message.='<td class="subtitle" style="color:gray;font-style:italic;">Error Code: </td>
			<td class="subdata '.$class.'" style="font-weight:bold;color:red;">'.$code.'</td>
			</tr>';
		}
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">Descriptor: </td>
		<td class="subdata" style="font-weight:bold;color:black;">'.$content_array['descriptor'].'</td>
		</tr>';
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">First 6 digit: </td>
		<td class="subdata" style="font-weight:bold;color:black;font-style:italic;">'.$content_array['first6'].'</td>
		</tr>';
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">Last 4 digit:</td>
		<td class="subdata" style="font-weight:bold;color:black;font-style:italic;">'.$content_array['last4'].'</td>
		</tr>';
		/*
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">Card Issuer:</td>
		<td class="subdata" style="font-weight:bold;color:black;">'.$content_array['bank'].'</td>
		</tr>';*/
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">Amount Purchase:</td>
		<td class="subdata" style="font-weight:bold;color:black;">'.number_format(floatval($content_array['purchase']),2).'</td>
		</tr>';
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">Date of Purchase:</td>
		<td class="subdata" style="font-weight:bold;color:black;">'.$content_array['date-of-purchase'].'</td>
		</tr>';
		$message.='<tr>
		<td class="subtitle" style="color:gray;font-style:italic;">Email Address:</td>
		<td class="subdata" style="font-weight:bold;color:black;">'.$content_array['email-address'].'</td>
		</tr>
		</table>
		';
		
		mail($to,$subject,$message,$from);
		
	}
		// email auth_api 20160110 - mikz
	public static function auth_email($to,$subject,$message,$from,$ishtml, $bccList = [])
	{
		$url='https://www.ican4consumers.com/mail/mail_api.php';
		$apikey='9d3bf9fe2ad81118bc5710274f962a16bee26722';

        $post = [
            "apikey" => $apikey,
            "from-email" => $from,
            "from-name" => $from,
            "to-email" => $to,
            "subject" => $subject,
            "message" => $message,
        ];

        if($ishtml==1){
            $post["html"] = 'true';
        }

        if(!empty($bccList)){
            $post["bcc-email"] = $bccList;
        }

//        $html='';

//		if($ishtml==1)
//		{
//			$html='&html=true';
//		}

//        echo print_r($post, true);
//		$post='apikey='.$apikey.'&from-email='.$from.'&from-name='.$from.'&to-email='.$to.'&subject='.$subject.'&message='.$message.$html;
		Api_post::_curl($url, http_build_query($post));

	}
}