<?php

class currencies_opt
{
	protected $url = "https://www.ican4consumers.com/ic4c-ajax/api/api_list_currencies.php?type=list-currency";
	
	public function __construct($array_params)
	{
		$method = $array_params['method'];
		$func_type  = $method['func-type'];
		
		switch($func_type)
		{
			case "get-list": //CAN BE USE ALSO FOR VERIFYING DESCRIPTOR
				$this->_getlist();
			break;

		}	
	}
	private function _getlist()
	{
		$result = @file_get_contents($this->url);

		if($result!='' || !is_null($result))
		{
			$response_data = json_decode($result,true);
			SharedResponse::query_response(4.5,$response_data);
		}
	}
}
