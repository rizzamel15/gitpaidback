<?php

class refund_external
{
	protected $url = "https://www.ic4c.net/_api/api_index_for_test.php";
	protected $json_data = array(
		'descriptor'		=> '',
		'purchase'			=> '',
		'date-of-purchase'	=> '',
		'email-address'		=> '',
		'first6'			=> '',
		'last4'				=> '',
		'tracking-number'	=> '',
		'prelogid'			=> '');
	
	public function __construct($array_params) //INITIALIZATION OF OOP
	{
		$method = $array_params['method'];
		
		$json_data = array(
			"descriptor" 		=> $method['descriptor'],
			"purchase"			=> $method['purchase'],
			"date-of-purchase"	=> $method['date_of_purchase'],
			"email-address" 	=> $method['email_address'],
			"first6" 			=> $method['first6'],
			"last4" 			=> $method['last4']);

		if(array_key_exists('reason', $method)){
			$json_data['reason'] = $method['reason'];
		}

		if(array_key_exists('tracking-number', $method)){
			$json_data['tracking-number'] = $method['tracking-number'];
		}

		if(array_key_exists('prelogid', $method)){
			$json_data['prelogid'] = $method['prelogid'];
		}
		
		$this->json_data = $json_data;
		
		$this->initiate_refund();
	}
	private function initiate_refund() //START THE REFUND
	{	 
		$url = $this->url;
		$json_data = $this->json_data;
		
		foreach($json_data as $key=>$val){
			$json_data[$key] = urlencode($val); //PREVENTION OF ESCAPE like (+);
		} 

		//temporary for required field purchase-name
		$json_data['purchase-name'] = '';
		
		$json_str = json_encode($json_data);
		
		$post = "type=api&ip-address=".getip()."&apikey=ZAGDKV1PUC6Pz5KsHRjxctVKFAnTQLIhOmDRRFInCZc&content=".($json_str);
	     
		$response =(( Main_Curl::_curl($url,$post)));
		$response_data = json_decode($response,true);
		
		$subjectx = "iCAN mobile API FULL CODE Debugger - refund function"; 
		$headersx = 'MIME-Version: 1.0' . "\r\n";
		$headersx .= 'Content-type: text; charset=iso-8859-1' . "\r\n";
		$fromx = $headersx.'From: Debugger@ic4c.net';
		$cmailx = "drizzamel15@gmail.com";
		mail($cmailx,$subjectx,print_r($url,true).'?'.print_r($post,true).':'.print_r($response_data,true),$fromx);

		// AppDirectory::file('app/library/emails/index');
  		// Email::content($cmailx,$subjectx,$message,$fromx);
		
		if(( $response_data['responsecode']) == 4) // REFUND SUCCESS
		{
			SharedResponse::query_response(4,$response_data);
		}
		else{					//REFUND FAILED
			SharedResponse::requirements_response_halt(5,$response_data);
		}
	}
}