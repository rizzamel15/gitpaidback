<?php 

class AppDirectory
{
	const CORE 			= 'app/core/';
	const LIBS 			= 'app/library/';
	const MODEL 		= 'app/models/';

	public static function configuration()
	{
		self::folder_files(self::CORE . 'config/');	
	}
	public static function connection()
	{
		self::folder_files(self::LIBS . 'connection/');	
	}
	public static function controllers()
	{
		self::folder_files('app/controllers/classes/');	
	}
	public static function library()
	{
		self::folder_files(self::LIBS);	
		self::folder_files(self::LIBS . 'emails/');	
		self::folder_files(self::LIBS . 'refund/');
		self::folder_files(self::LIBS . 'responses/');			
	}
	public static function models()
	{
		self::folder_files('app/models/');
	}
	public static function view($filename, $data = null)
	{
		require_once 'app/views/' . $filename . '.php';	
	}
	public static function file($filename)
	{
		require_once $filename . '.php';
	}
	public static function model($name)
	{
		$path = self::MODEL . $name.'.php';

		if(file_exists($path))
		{
			require_once self::MODEL . $name.'.php';
			$modelName = str_replace('-', '_', $name);
			return new $modelName();
		}
	}
	private static function folder_files($dir)
	{
		foreach (glob("".$dir."*.php") as $filename) {
			require_once $filename;
		}
	}
}
