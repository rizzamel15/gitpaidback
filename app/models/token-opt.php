<?php
ob_start();
error_reporting(0);

class Token_Opt extends Model
{
	public function _get($array_params)
	{
		$method = $array_params['method'];
		
		$methodpassword= md5_new::_adv($method['password']);

		$query_data = $this->db->select("SELECT * FROM tempic4c.pre_login WHERE USERNAME = :username AND PASSWORD = :password limit 1", array(":username" => $method['username'], ":password" => $methodpassword));
		
		if(count($query_data)>=1) {
			if($query_data[0]['VERIFIED']!='1'){//account verification
				SharedResponse::requirements_response_halt(8);
			}
		}
		
		$bool = false;
		$account_details = array();

		if(count($query_data)>=1)
		{
			if($query_data[0]['USERNAME'] == $method['username'] && $query_data[0]['PASSWORD'] == $methodpassword)
			{
				$bool = true;
				$not_included_array = array('PASSWORD','DATE','IP','MD5','VERIFIED','NOTIF_PROMO','NOTIF_NEWMERCH','FATTEMPTS','CPASS');

				foreach($query_data[0] as $key=>$val)
				{
					if(!in_array($key,$not_included_array))
					{
						$account_details[$key] = $val;
					}
				}
				
				$account_details['travertex']='';
			}
		}

		return array("bool" => $bool , "account_details" => $account_details);
	}
}

ob_end_flush();