<?php
ob_start();
error_reporting(0);

class Query_Retrieval extends Model
{
	public function _get_conversion($array_params)
	{
		return $this->db->select("SELECT * FROM tempic4c.currency_historicalrates WHERE substr(date_on_record,1,10) = :conversion_date", array(":conversion_date" => $array_params['conversion_date']));
	}
	
	public function _get_descriptor_type($array_params)
	{
		return $this->db->select("SELECT extra_field FROM tempic4c.merchantid WHERE mid =(SELECT DESCRIPTOR_MID FROM tempic4c.descriptor WHERE descriptor_text = :descriptor_text)", array(":descriptor_text" => $array_params['descriptor']));
	}
	
	public function _get_history($pre_login_id,$extra_query)
	{	
		return $this->db->select("SELECT request_id AS 'Confirmation Code', cardno AS 'Card number', email AS 'Email Address', amount AS 'Amount of Purchase', response AS 'Refund Status', date AS 'Refund Request Date', time_purchase AS 'Date of Purchase', ur_descriptor AS 'Descriptor or MID' FROM tempic4c.userrequest WHERE pre_login_id = :pre_login_id ".$extra_query, array(":pre_login_id" => $pre_login_id));
		
	}
	public function _get_profile($pre_login_id)
	{
		$query_data = $this->db->select("SELECT * FROM tempic4c.pre_login WHERE PRE_LOGIN_ID = :pre_login_id", array(":pre_login_id" => $pre_login_id));

		$profile_details = $query_data[0];
		$profile_details['EMAIL-OTHER'] = $this->_get_other_email($pre_login_id);

		return $profile_details;
	}
	public function _get_other_email($pre_login_id)
	{
		$other_email = array();

		$query_data = $this->db->select("SELECT EMAIL_ID, EMAILADDRESS FROM tempic4c.tblemailbank WHERE PRE_LOGIN_ID = :pre_login_id", array(":pre_login_id" => $pre_login_id));
		
		if(count($query_data)>0){

			$counter=0;
			while(count($query_data)>$counter)
			{
				$fetch_email = array(
					'EMAIL_ID'=>$query_data[$counter]['EMAIL_ID'], 
					'EMAILADDRESS' => $query_data[$counter]['EMAILADDRESS']);

				array_push($other_email,$fetch_email);
				$counter++;
			}
		}
		else{
			$other_email='';
		}

		return $other_email;
	}
	public function _get_account_name($pre_login_id)
	{
		$query_data =  $this->db->select("SELECT FIRSTNAME, LASTNAME FROM tempic4c.pre_login WHERE PRE_LOGIN_ID = :pre_login_id", array(":pre_login_id" => $pre_login_id));

		return $query_data[0]['FIRSTNAME'] . '&nbsp;' .$query_data[0]['LASTNAME'];
	}
}

ob_end_flush();