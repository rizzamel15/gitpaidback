<?php 

ob_start();
error_reporting(0);

class ErrorFile
{
	public static function index()
	{
		AppDirectory::view('not-found');
	}
}

ob_end_flush();
