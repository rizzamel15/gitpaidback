<?php

class Output
{
	public static function xss($val)
	{
		$array_from_replace = array('<','>','/','"');
		$array_to_replace = array('','','','');
		$new_val = str_replace($array_from_replace,$array_to_replace,$val);
		return  htmlspecialchars($new_val);
	}
	public static function _date_str($date)
	{
		$date_array =getdate(strtotime($date));
		$date = $date_array['month'] .' ' .$date_array['mday'] . ', ' . $date_array['year'];
		return $date;
	}
	public static function replace($array_params)
	{
		$access_type = $_SESSION['api-type'];
		
		$response_str = $array_params['responsestr'];
		$content_array = $array_params['content-array'];
		$result_array = $array_params['result-array'];
		
		//--[CONFIRMATION NUMBER]--
		$old_array = array('--[DATE]--','--[AMOUNT]--','--[LAST4]--','--[CONFIRMATION NUMBER]--','--[ERROR CODE]--','--[DESCRIPTOR]--');
		
		$date = self::_date_str($content_array['date-of-purchase']);
		
		$content_array['purchase'] = number_format(floatval($content_array['purchase']),2);

		switch($access_type)
		{
			case "WEB":
				$new_array = array(
					'['.$date.']',
					'['.$content_array['purchase'].']',
					'['.$content_array['last4'].']',
					'('.$array_params['confirmation-number'].')',
					'['.$array_params['error-code'].']',
					'['.$content_array['descriptor'].']'
					);
					
				foreach($new_array as $key=>$val){$new_array[$key] =(self::xss($val));}
					
			break;
			case "MOBILE":
				$new_array = array($date,$content_array['purchase'],$content_array['last4'],$array_params['confirmation-number'],$array_params['error-code'],$content_array['descriptor']);
			break;
			case "API":
				$error_code = '';
				if(isset($array_params['error-code']))
				{
					$error_code = $array_params['error-code'];
					
				}
				$confirmation_number = '';
				
				if(isset($array_params['confirmation-number']))
				{$confirmation_number = $array_params['confirmation-number'];}
				
				$new_array = array($date,$content_array['purchase'],$content_array['last4'],$confirmation_number,$error_code,$content_array['descriptor']);
			break;
			case "CSS":
								$new_array = array(
					'['.$date.']',
					'['.$content_array['purchase'].']',
					'['.$content_array['last4'].']',
					'('.$array_params['confirmation-number'].')',
					'['.$array_params['error-code'].']',
					'['.$content_array['descriptor'].']'
					);
					
				foreach($new_array as $key=>$val){$new_array[$key] =(self::xss($val));}
			break;
			
		}
		return str_replace($old_array,$new_array,$response_str);
	}

	public static function escapeJsonString($value) 
	{ # list from www.json.org: (\b backspace, \f formfeed)
	    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
	    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
	    $result = str_replace($escapers, $replacements, $value);
	    return $result;
	}

	// public function output_json($array_params)
	// {
	// 	$array_response= $array_params['array-response'];


	// 	$connection = new connection;
	// 	$main_response = new main_response;
	// 	$email_class = new email;

	// 	$connection->_disconnect();


	// 	exit(json_encode($array_response));

	// }
	public static function json($array_params)
	{
		// $connection = new connection;
		// $main_response = new main_response;
		// $email_class = new email;
		
		$type = $array_params['type'];
		
		$data = $array_params['data'];

		if($type=='success')
		{
			$subtype = $array_params['sub-type'];
			$content_array = $array_params['content-array'];
			$result_array = $array_params['result-array'];
			$array_response = Refund_Main_Response::_final_response($subtype);


			$confirmation_number = $array_params['confirmation-number'];
			$content_array['confirmation-number'] = $confirmation_number;



			//***********************************		
			//***********************************
			if(!isset($_SESSION['j-update']))
				{
					Refund_Email::_success_error(array('type'=>$type,'code'=>$confirmation_number,'content-array'=>$content_array));
			}
			//***********************************		
			//***********************************



			$date = self::_date_str($content_array['date-of-purchase']); 
			$content_array['date-of-purchase'] = $date;
			
			$responsedetails = json_encode($content_array);
			$array_response['responsestr'] = rawurlencode(self::replace(array('responsestr'=>$array_response['responsestr'],'content-array'=>$content_array, 'result-array'=>$result_array,'confirmation-number'=>$confirmation_number)));	
			
			$array_response['response-details']=($responsedetails);


				//exit($array_response['responsestr']);

				
				
		}
		else
		{
			$subtype = $array_params['sub-type'];
			$array_response = Refund_Main_Response::_final_response($subtype);
			
					
					$content_array = $array_params['content-array'];
					$result_array = $array_params['result-array'];
					$error_code = '';
					
					if(isset($array_params['error-code'])){
						$error_code = $array_params['error-code'];
							if(!isset($_SESSION['j-update']))
							{
								Refund_Email::_success_error(array('type'=>$type,'code'=>$error_code,'content-array'=>$content_array));
							}
						}
						
				
					$array_response['errorstr'] = self::replace(array('responsestr'=>$array_response['errorstr'],'content-array'=>$content_array, 'result-array'=>$result_array,'error-code'=>$error_code));
			
			
		};
			
			
		// $this->output_json(array('array-response'=>$array_response));
		Response::json_output(array('array-response'=>$array_response));
		
		
	}
}