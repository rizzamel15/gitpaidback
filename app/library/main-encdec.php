<?php

class Main_Encdec
{
	public static function encrypturl($val)
	{	
		$array = str_split($val,1);
		$val='';
		
		for($x=0;$x<=sizeof($array)-1;$x++){
			$val .= " " . decbin((ord($array[$x]) + 15));
		}
		
		$val = base64_encode($val);
		return $val;
	}
	public static function decrypturl($val)
	{
		$val = base64_decode($val);
		$array = explode(' ',$val);
		$val = '';
		
		for($x=1;$x<=sizeof($array)-1;$x++){
			$val .=chr((bindec($array[$x]))-15);
		}
	
		return $val;
	}
}
