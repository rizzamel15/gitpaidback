<?php

class Response
{
	public static function json_output($response)
	{		
		header("content-type: application/json");
        exit(json_encode($response));
	}
}