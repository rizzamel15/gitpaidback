<?php 

class Account
{
	public static function sign_in($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$login = AppDirectory::model('account-login');
		$result = $login->consumer_login($content);

		if (count($result) >= 1 )
		{
			session_start();
			$_SESSION['first-name'] = $result[0]['FIRSTNAME'];
			$_SESSION['last-name'] = $result[0]['LASTNAME'];
			$_SESSION['mobile-token'] = $result[0]['mobile_token'];
			unset($result[0]['PASSWORD']);
			
			$response_data = array(
				'first-name' 	=> $result[0]['FIRSTNAME'],
				'last-name' 	=> $result[0]['LASTNAME'], 
				'mobile-token' 	=> $result[0]['mobile_token']);

			SharedResponse::query_response(19,$result[0]);
			
		}else{
			SharedResponse::query_response(12);
		}
	}
	public static function get_profile($request_type,$token)
	{
		$content['token'] = $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$query_retrieval = AppDirectory::model('query-retrieval');
		$array_response = $query_retrieval->_get_profile($consumer_log_id);
		Response::json_output($array_response);
	}
	public static function check_username($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$check_data = AppDirectory::model('check-data');
		$result = $check_data->_check_username_availability($content['username']);

		$array_params = array(
			'type' 			=> $result['type'],
			'query-data' 	=> $result['query-data'],
			'num1' 			=> 6,
			'num2' 			=> 1,
			'num3' 			=> 2
		);

		self::_shared_responses($array_params);	
	}
	public static function account_signup($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$account_creation = AppDirectory::model('account-creation');
		$array_response = $account_creation->full_register($content);	
		Response::json_output($array_response);
	}
	public static function account_recovery($content)
	{
		$recovery_type = $content['recovery_type'];

		switch($recovery_type)
		{
			case 'forgot-username':
				$check_data = AppDirectory::model('check-data');
				$result = $check_data->_check_email_availability($content['email_address'],2);

				$array_params = array(
					'type' 			=> $result['type'],
					'query-data' 	=> $result['query-data'],
					'num1' 			=> 7,
					'num2' 			=> 4,
					'num3' 			=> 5
				);

				self::_shared_responses($array_params);

				$request_type = 'forgot_username';
				Requirements::_verify_action($content,$request_type);
				$account_recovery = AppDirectory::model('account-recovery');
				$query_data = $account_recovery->forgot_username($content);

				// mail('drizzamel15@gmail.com','Debugger',print_r($query_data,true),'From:donotreply@debugger.com');

				if(count($query_data)>=1){
		        	AppDirectory::view('emails/forgot-username', $query_data[0]);

				}else{
					SharedResponse::check_response_halt(8);
				}
			break;
			
			case 'forgot-password':
				$check_data = AppDirectory::model('check-data');
				$result = $check_data->_check_username_availability($content['username'],2);

				$array_params = array(
					'type' 			=> $result['type'],
					'query-data' 	=> $result['query-data'],
					'num1' 			=> 6,
					'num2' 			=> 1,
					'num3' 			=> 2
				);

				self::_shared_responses($array_params);

				$request_type = 'forgot_password';
				Requirements::_verify_action($content,$request_type);
				$account_recovery = AppDirectory::model('account-recovery');
				$account_recovery->forgot_password($content);
			break;
			
			default:
				SharedResponse::requirements_response_halt(12);
			break;
		}
	}
	public static function add_email($content,$request_type,$token)
	{
		$content['token']= $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$query_insert_update = AppDirectory::model('query-insert-update');

		foreach ($content['email_address'] as $email_address) {
			$query_insert_update->_add_other_emails($email_address,$consumer_log_id);
		}

		SharedResponse::query_response(1);
	}
	public static function delete_other_email($content,$request_type)
	{
		Requirements::_verify_action($content,$request_type);
		$check_data = AppDirectory::model('check-data');
		$email_id = $check_data->_check_other_email($content);

		if(count($email_id)==0){
			SharedResponse::check_response_halt(7);
		}

		$query_insert_update = AppDirectory::model('query-insert-update');
		$query_insert_update->_delete_other_email($email_id);	

		SharedResponse::query_response(11);
	}
	public static function modify_account_name($content,$request_type,$token)
	{
		$content['token']= $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$nonmember_class = AppDirectory::model('nmm-class');
		$fc_id = $nonmember_class->nmm_verify_token($token);
		$check_data = AppDirectory::model('check-data');
		$query_data = $check_data->_check_account_name($content);

		if(count($query_data)==0){
			SharedResponse::check_response_halt(0);
		}

		$query_insert_update = AppDirectory::model('query-insert-update');
		$query_insert_update->_update_account_name($content,$consumer_log_id,$fc_id);

		SharedResponse::query_response(8);
	}
	public static function modify_main_email_address($content,$request_type,$token)
	{
		$content['token']= $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$nonmember_class = AppDirectory::model('nmm-class');
		$check_data = AppDirectory::model('check-data');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$fc_id = $nonmember_class->nmm_verify_token($token);
		$query_data = $check_data->_main_email_address($content);

		if(count($query_data)==0){
			SharedResponse::check_response_halt(0);
		}

		$email_address = $content['new_main_email_address'];
		$email_check = $check_data->_check_email_availability($email_address);
		
		if(count($email_check['query-data'])>0){
			$array_response = Main_Response::check_response_halt(4);
			Response::json_output($array_response);
		}

		$query_insert_update = AppDirectory::model('query-insert-update');
		$query_insert_update->_update_main_email_address($content,$consumer_log_id,$fc_id);

		SharedResponse::query_response(10);
	}
	public static function modify_password($content,$request_type,$token)
	{
		$content['token']= $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$nonmember_class = AppDirectory::model('nmm-class');
		$fc_id = $nonmember_class->nmm_verify_token($token);
		$check_data = AppDirectory::model('check-data');
		$query_data = $check_data->_check_password($content);

		if(count($query_data)==0){
			SharedResponse::check_response_halt(0);
		}

		$query_insert_update = AppDirectory::model('query-insert-update');
		$query_insert_update->_update_password($content,$consumer_log_id,$fc_id);

		SharedResponse::query_response(2);
	}
	public static function modify_username($content,$request_type,$token)
	{
		$content['token']= $token;
		Requirements::_verify_action($content,$request_type);
		$mobile_token = AppDirectory::model('mobile-token');
		$consumer_log_id = $mobile_token->_verify_token($token);
		$nonmember_class = AppDirectory::model('nmm-class');
		$fc_id = $nonmember_class->nmm_verify_token($token);
		
		$check_data = AppDirectory::model('check-data');
		$query_data = $check_data->_check_username($content);

		if(count($query_data)==0){
			SharedResponse::check_response_halt(0);
		}

		$username = $content['new_username'];
		$username_check = $check_data->_check_username_availability($username);
		$fetch_data = $username_check['query-data'];

		if(count($fetch_data)>0){
			SharedResponse::check_response_halt(1);
		}

		$query_insert_update = AppDirectory::model('query-insert-update');
		$query_insert_update->_update_username($content,$consumer_log_id,$fc_id);

		SharedResponse::query_response(9);
	}
	public static function sign_out()
	{
		$array_response = array('responsecode'=>1,'responsestring'=>'Successfully logged out.');
		Response::json_output($array_response);
		session_destroy();
	}
	private static function _modify_shared_query($array_params)
	{
	 // 	$token = $array_params['token'];
	 // 	$request_type = $array_params['request_type'];
	 // 	$content = $array_params['content'];
	 // 	$function_name = $array_params['function_name'];
	 // 	$responsecode = $array_params['responsecode'];

		// $content['token']= $token;
		// Requirements::_verify_action($content,$request_type);
		// $mobile_token = AppDirectory::model('mobile-token');
		// $consumer_log_id = $mobile_token->_verify_token($token);
		// $nonmember_class = AppDirectory::model('nmm-class');
		// $fc_id = $nonmember_class->nmm_verify_token($token);
		// $query_insert_update = AppDirectory::model('query-insert-update');
		// $query_insert_update->$function_name($content,$consumer_log_id,$fc_id);

		// SharedResponse::query_response($responsecode);
	}
	private static function _shared_responses($array_params)
	{
		$type = $array_params['type'];
		$query_data = $array_params['query-data'];
		$num1 = $array_params['num1'];
		$num2 = $array_params['num2'];
		$num3 = $array_params['num3'];

		if($type==2){
			if(count($query_data)<1){
				SharedResponse::check_response_halt($num1);
			}
		}
		else{
			if(count($query_data)>0){
				SharedResponse::check_response_halt($num2);
			}
			else
			{
				if($type==null){
					SharedResponse::check_response_halt($num3);
				}
			}
		}
	}
}
