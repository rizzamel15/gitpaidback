<?php

class Requirements
{
	public static function _check_json($array_params)
	{
		$ob = json_decode($array_params,true);
		
		if($ob===null) {
			SharedResponse::requirements_response_halt(0);
		}
	}
	public function _check_content($array_params,$key_check){}
	public static function _check_token($array_params)
	{
		if(($array_params['consumer_user']=='')||($array_params['consumer_user']=='')||is_null($array_params['consumer_pass'])||is_null($array_params['consumer_pass']))
			{
				SharedResponse::requirements_response_halt(1);
			}
		
		return $array_params; 
	}
	public static function _verify_action($array_params,$request_type)
	{	
		switch($request_type)
		{
			case "add_email":
				$required_array = array('token', 'email_address');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			case "get_token":
				$required_array = array('username','password');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			case "get_profile":
				$required_array = array('token');
				self:: checking_parameters($required_array, $array_params, 3);
			break;
			
			case "sign_in":
				$required_array = array('username','password');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			case "modify_other_emails":
				$required_array = array('token');
				self:: checking_parameters($required_array, $array_params, 3);
			break;
			
			case "get_history":
				$required_array = array('token');
				self:: checking_parameters($required_array, $array_params, 3);
			break;
			
			case "modify_password":
				$required_array = array('token','old_password','new_password');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			case "check_username":
				$required_array = array('username');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			case "account_signup":
				$required_array = array(
							'first_name',
							'last_name',
							'main_email_address',
							'phone_number',
							'username',
							'password'
							);

				self:: checking_parameters($required_array, $array_params, 1);
			break;
		
			case "refund":
				$required_array = array(
							'descriptor',
							'purchase',
							'date_of_purchase',
							'email_address',
							'first6',
							'last4',
							'token'
							);
			      
				self:: checking_parameters($required_array, $array_params, 1);
			break;

			case "get_descriptor":
				$required_array = array('descriptor');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			case "tracking_descriptor":
				$required_array = array('descriptor');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			case "get_conversion":
				$required_array = array('conversion_date');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			case "forgot_username":
				$required_array = array('email_address');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			case "forgot_password":
				$required_array = array('username');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
	
			case "nmm_refund":
				$required_array = array(
                                       'merchant-info',
                                       'merchant-business',
                                       'merchant-website',
                                       'merchant-email',
                                       'merchant-phone',
                                       'consumer-firstname',
                                       'consumer-lastname',
                                       'consumer-email',
                                       'consumer-phone',
									   'consumer-cardfirst',
									   'consumer-cardlast',
                                       'purchase-date',
                                       'purchase-amount',
                                       'purchase-email',
                                       'purchase-reason',
                                       'purchase-product',
                                       );
				
				$new_required_array = array();
				for($x=0; $x < count($required_array); $x++)
				{
					if(($required_array[$x]!='merchant-info')&&($required_array[$x]!='consumer-firstname')&&($required_array[$x]!='consumer-lastname')&&($required_array[$x]!='purchase-date')&&($required_array[$x]!='purchase-amount')&&($required_array[$x]!='purchase-email')&&($required_array[$x]!='purchase-reason')&&($required_array[$x]!='purchase-product'))
					{
						$key = $required_array[$x];
						
						if($array_params[$key]!=""){
							array_push($new_required_array,$required_array[$x]);
						}
					}
					else{
						array_push($new_required_array,$required_array[$x]);
					}
				}
				
				$new_array_params = array();

				foreach($array_params as $key=>$val)
				{
					if($array_params[$key]!=''){
						$new_array_params[$key] = $array_params[$key];
					}
				}

				self:: checking_parameters($new_required_array, $new_array_params, 1);
			break;

			case "nmm_get_history":
				$required_array = array('token');
				self:: checking_parameters($required_array, $array_params, 3);
			break;

			case "modify_account_name":
				$required_array = array(
					'token',
					'old_firstname',
					'old_lastname',
					'new_firstname',
					'new_lastname');
				self:: checking_parameters($required_array, $array_params, 1);
			break;

			case "modify_username":
				$required_array = array(
					'token',
					'old_username',
					'new_username');
				self:: checking_parameters($required_array, $array_params, 1);
			break;

			case "modify_main_email_address":
				$required_array = array(
					'token',
					'old_main_email_address',
					'new_main_email_address');
				self:: checking_parameters($required_array, $array_params, 1);
			break;

			case "delete_other_email":
				$required_array = array(
					'email_id',
					'email_address');
				self:: checking_parameters($required_array, $array_params, 1);
			break;
			
			default:
				SharedResponse::requirements_response_halt(4); //UNDEFINE ACTION
			break;
		}
	}
	private static function checking_parameters($required_array, $array_params, $code)
	{
		$is_missing_exist = checking_tempo::_check_missing_params(
					array(
							'required-array'   => $required_array,
							'collection-array' => $array_params),true);

		$is_empty_exist = checking_tempo::_check_null_params(
					array(
							'collection-array' => $array_params),true);
	
		//STATIC FOR CHECKINGS PARAMENTER
		if($is_missing_exist==true || $is_empty_exist==true)
		{
			SharedResponse::requirements_response_halt($code);
		}
	}
}