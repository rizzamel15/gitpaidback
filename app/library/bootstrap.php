<?php

class Bootstrap
{
	private static $_url 			= null;
	private static $controller 		= null;
	private static $controllerPath 	= 'app/controllers/';
	private static $modelPath 		= 'app/models/';
	private static $errorFile 		= 'error-file.php';
	private static $defaultFile		= 'init.php';

	public static function _init()
	{
		self::_getUrl();

		if(empty(self::$_url[0]))
		{
			self::_loadDefaultController();
			return false;
		}

		self::_loadExistingController();
		self::_callControllerMethod();
	}
	public static function setControllerPath($path)
	{
		self::$controllerPath = trim($path, '/') . '/';
	}
	public static function setModelPath($path)
	{
		self::$modelPath = trim($path, '/') . '/';
	}
	public static function setErrorFile($path)
	{
		self::$errorFile = trim($path, '/');
	}
	public static function setDefaultFile($path)
	{
		self::$defaultFile = trim($path, '/');
	}
	private static function _getUrl()
	{
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = filter_var($url, FILTER_SANITIZE_URL); //to sanitize the url.
		self::$_url = explode('/', $url);
	}
	private static function _loadDefaultController()
	{
		$file = self::$controllerPath . self::$defaultFile;
		require_once $file;
		self::$controller = ApiMain::index();
	}
	private static function _loadExistingController()
	{
		$file = self::$controllerPath . self::$_url[0] . '.php';

		if(file_exists($file))
		{
			require_once $file;
			$new_url = str_replace('-', '', self::$_url[0]);
			self::$controller = new $new_url;
		}
		else
		{
			self::_error();
			return false;
		}
	}

	/**
	 * If a method is passed in the GET url parameter
	 * This will explain how below codes are running
	 * http://localhost/controller/method/(param)/(param)/(param)
	 * url[0] = Controller name
	 * url[1] = Method
	 * url[2] = Param
	 * url[3] = Param
	 * url[4] = Param
	 */
	private static function _callControllerMethod()
	{
		$url_replace = str_replace('-', '_', self::$_url);

		$length = count($url_replace);

		// make sure the method we are calling exists
		if($length > 1)
			if(!method_exists(self::$controller, $url_replace[1]))
				self::_error();

		// Determine what to load
		switch ($length) 
		{
			case '5':
				self::$controller->{$url_replace[1]}($url_replace[2], $url_replace[3], $url_replace[4]);
				break;

			case '4':
				self::$controller->{$url_replace[1]}($url_replace[2], $url_replace[3]);
				break;

			case '3':
				self::$controller->{$url_replace[1]}($url_replace[2]);
				break;

			case '2':
				self::$controller->{$url_replace[1]}();
				break;
			
			default:
				self::$controller->index();
				break;
		}
	}

	private static function _error()
	{
		$error_file = self::$controllerPath . self::$errorFile;
		require_once $error_file;
		self::$controller = ErrorFile::index();
		exit;
	}
}