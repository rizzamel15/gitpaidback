<?php

class SharedResponse
{
	public static function query_response($code,$extra_params = null)
	{		
		$array_response = Main_Response::_query_response($code,$extra_params);
		Response::json_output($array_response);
	}
	public static function check_response_halt($code)
	{		
		$array_response = Main_Response::check_response_halt($code);
		Response::json_output($array_response);
	}
	public static function requirements_response_halt($code)
	{
		$array_response = Main_Response::requirements_response_halt($code);
		Response::json_output($array_response);
	}
}