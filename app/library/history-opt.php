<?php

class History_Opt
{
	public static function _query_generator($array_params)
	{
		$return_query = ''; 
		 
		if(count($array_params)==0){return '';}
		
		foreach($array_params as $key=>$val)
		{
			switch($key)
			{
				case "search_criteria":
					$return_query .= self::_interpret_query($val);
				break;
				
				case "sort":
					$return_query .= self::_interpret_query(array($val=>$val));
				break;
				
				case "order":
					$return_query .= self::_interpret_query(array($val=>$val));
				break;
				
				default:
					SharedResponse::check_response_halt(3);
				break;
			}
		}
		
		return $return_query;
	}
	public static function _interpret_query($array_params)
	{
		$where_list = array();
		$orderby = '';
		$ordertype = '';
		
		foreach($array_params as $key=>$val)
		{
			
			switch($key)
			{
				case "confirmation_number":
					array_push($where_list,"request_id = ".$val);
				break;
				
				case "descriptor":
					array_push($where_list,"ur_descriptor = '".$val."'");
				break;
				
				case "amount":
					array_push($where_list,"amount = ".$val);
				break;
				
				case "date_requested":
					array_push($where_list,"date(date) = '".$val."'");
				break;
				
				case "email_address":
					array_push($where_list,"email = '".$val."'");
				break;
				
				case "sort_amount":
					$orderby=' ORDER BY amount';
				break;
				
				case "sort_date_requested":
					$orderby=' ORDER BY date';
				break;
				
				case "sort_email_address":
					$orderby=' ORDER BY email';
				break;
				
				case "asc":
					$ordertype = ' ASC';
				break;
				
				case "desc":
					$ordertype = ' DESC';
				break;
				
				default:
					SharedResponse::check_response_halt(3);
				break;
			}
		}	
		
		$query = self::_process_query($where_list,$orderby,$ordertype);
		
		return $query;
	}
	public static function _process_query($where_list,$orderby,$ordertype)
	{
		$query='';
		$where_count = 0;
		
		while(count($where_list)>$where_count)
		{
			if($where_count==0){$query.=' AND ';}
			if($where_count>0){$query.=' AND ';}
			$query.=$where_list[$where_count];
			$where_count++;
		}
		
		return $query.''.$orderby.''.$ordertype;
	}
}