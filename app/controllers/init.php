<?php 
ob_start();
error_reporting(0);
header("Access-Control-Allow-Origin: *");

class ApiMain
{
	public static function index()
	{
		$method = $_REQUEST;
		// exit(print_r($method));

		// require 'data.php';
		// $method = $post_array;//for testing

		if(!isset($method['action_type'])){exit();}

		if(array_key_exists('api_key', $method)){
			$api_key = $method['api_key'];
			API_Check::_api_key($api_key);
		}else{
			SharedResponse::requirements_response_halt(10);
		}

		$request_type = $method['action_type'];
		$token = array_key_exists('token', $method) ? $method['token'] : '';	

		if(array_key_exists('content', $method)){
			if(($request_type!='get_profile')&&($request_type!='get_history')){
				Requirements::_check_json(trim($method['content']));
			}

			$content = json_decode(trim($method['content']),true);
		}

		switch($request_type)
		{
			case "account_recovery":
				Account::account_recovery($content);
			break;
			case "account_signup":
				Account::account_signup($content,$request_type);
			break;
			case "add_email":
				Account::add_email($content,$request_type,$token);
			break;
			case "change_token":
				Token::change_token($content);
			break;
			case "check_username":
				Account::check_username($content,$request_type);
			break;
			case "delete_other_email":
				Account::delete_other_email($content,$request_type);
			break;
			case "get_conversion":
				Currency::get_conversion($content,$request_type);
			break;
			case "get_currency_list":
				Currency::get_currency_list();
			break;
			case "get_descriptor":
				Descriptor::get_descriptor($content,$request_type);
			break;
			case "get_history":
				Refund::get_history($content,$request_type,$token);
			break;
			case "get_location":
				Location::get_location();
			break;
			case "get_profile":
				Account::get_profile($request_type,$token);
			break;
			case "get_token":
				Token::get_token($content,$request_type);
			break;
			case "modify_account_name":
				Account::modify_account_name($content,$request_type,$token);
			break; 
			case "modify_main_email_address":
				Account::modify_main_email_address($content,$request_type,$token);
			break;
			case "modify_password":
				Account::modify_password($content,$request_type,$token);
			break;
			case "modify_username":
				Account::modify_username($content,$request_type,$token);
			break;
			// case "nmm_get_history":
			// 	Refund::nmm_get_history($token,$request_type,$content);
			// break;
			// case "nmm_refund":
			// 	Refund::nmm_refund($content,$request_type,$token);
			// break;
			// case "refund":
			// 	Refund::_refund($content,$request_type,$token);
			// break;
			case 'sign_in':
				Account::sign_in($content,$request_type);
			break;
			case 'sign_out':
				Account::sign_out();
			break;
			case "tracking_descriptor":
				Descriptor::tracking_descriptor($content,$request_type);
			break;
			default:
				SharedResponse::requirements_response_halt(11);
			break;
		}
	}
}

AppDirectory::controllers();
ob_end_flush();
