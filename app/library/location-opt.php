<?php

class Location_opt
{
	public static function _get_location_with_currency($array_params=null)
	{
		$location_array = ip::_get_loc();
		$location_array['ip-address'] = ip::_get_ip();
		return Main_Response::_query_response("location-response-ok",$location_array);
	}
}