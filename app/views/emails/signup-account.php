<?php
$server = $data['server'];
$to = $data['main-email-address'];//real
// $to = 'drizzamel15@gmail.com';//temporary, for testing.
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$from = $headers.'From: confirmation-do-not-reply@ic4c.net';
$from = 'confirmation-do-not-reply@ic4c.net';
$subject ='iCAN4Consumers';

ob_start();
?>

<!DOCTYPE html>

<html style="font-family:arial;font-family: verdana; font-size:12px;line-height: 30px !important;color:black !important;">
<body>
                
    <div style="padding:5px;">
    	<h1>
    		<span class="marker">
    			<img alt="iCAN for Consumers" title="iCAN for Consumers"  style="width: 153px;height: 54px;" src="https://www.ican4consumers.com/wp-content/uploads/2015/02/iCan_Logo_Full-Color-10.png" /> 
    		</span>
    	</h1>
    	</div><?php'."\r\n".'?>

        <hr>
        
        <div style="padding:5px;">
           	<h1 style="text-align:center;color:#18426f; padding:20px;">
            	<span class="marker"  style="font-family:verdana;color:#18426f;">ACCOUNT VERIFICATION</span>
            </h1>
        </div><?php'."\r\n".'?>
           	
        <hr> 
                 
        <div>Greetings,</div><?php'."\r\n".'?>
        <br />

        <div style="padding:5px;"> Thank you for registering with iCAN4Consumers.</div><?php'."\r\n".'?>
                    
        <div style="padding:5px;">Please click on the iCAN logo below to verify your registration.</div><?php'."\r\n".'?>
                    
        <div style="padding:5px;">
            <h3 style="padding:10px;">
                <a href="<?= $server ?>" style="text-decoration: none;border-radius:2px;padding:10px;display:block;color: white; background: #18426f; font-weight:bold;width:auto;min-width:150px;text-align:center; font-size:15px;">Click to verify</a>
       	</div><?php'."\r\n".'?>
        </h3>
                    
        <div  style="padding:5px;">Your account details:</div><?php'."\r\n".'?>
        
        <table style='font-family: verdana;font-size:12px;border-collapse:collapse;float:none;border:solid 1px #EAEAEA;'>
            <tr>
                <td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                	<b>Firstname</b>:
                </td>
                <td style="padding:5px;"><?= $data['first-name'] ?></td>
            </tr>
            <tr>
                <td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                	<b>Lastname</b>:
                </td>
                <td style="padding:5px;"><?= $data['last-name'] ?></td>
            </tr>
                
            <tr>
                <td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                	<b>Username</b>:
                </td>
                <td style="padding:5px;"><?= $data['username'] ?></td>
            </tr>
		
            <tr>
                <td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                	<b>Primary Email</b>:
                </td>
                <td style="padding:5px;"><?= $data['main-email-address'] ?></td>
            </tr>
			<tr>
				<td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                	<b>Receive iCan Newsletter</b>:
                </td>
                <td style="padding:5px;"><?= $data['notification-newsletter'] ?></td>
            </tr>
			<tr>
				<td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                	<b>Receive Promotional</b>:
                </td>
                <td style="padding:5px;"><?= $data['notification-promo'] ?></td>
            </tr>
            <tr>
            <td style="padding:5px;text-align:right;border-right:solid 1px #EAEAEA;">
                <b>New Merchants</b>:
            </td>
            <td style="padding:5px;"><?= $data['notification-newmerch'] ?></td>
            </tr>
		</table>

        <div style="clear:both;"></div>

        <div style="padding:5px;">By clicking on the link provided above you also agree to our <a href="https://www.ic4c.net/aboutus.php?tac=1">Terms and Conditions</a>.</div><?php'."\r\n".'?>
                    
        <div style="padding:5px;">For inquiries, you may visit our <a href="https://www.ican4consumers.com/contact/">contact page</a></div><?php'."\r\n".'?>
            
        <div style="padding:5px;">or call our <b>iCAN Support Team Hotline</b> at <b>+1 855-660-3214.</b></div><?php'."\r\n".'?>
                   
        <div style="padding:5px;">Kind Regards,</div><?php'."\r\n".'?>
                    
        <div style="padding:5px;"> iCAN4Consumers Support Team</div><?php'."\r\n".'?>
                    
        <hr><?php'."\r\n".'?>
                    
        <div style="text-align:center;padding:5px;"><em>Do not reply to this autogenerated email.</em></div>
                    
        <hr>
     
    </body>
</html>
                
<?php 
$message = ob_get_clean();
$array_params = [$to,$subject,$message,$from];
$mail_response = Email::auth_mail($array_params);
Email::response($mail_response,$message,'Account has been registered successfully.');
?>