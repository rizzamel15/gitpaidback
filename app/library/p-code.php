<?php

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 
//           CURRENCY CODE          //
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 

class p_code
{
     public static function _get_country_code($array_params = null)
     {
          $country_code = self::$country_code;
       
          return $country_code;
     }
    protected static $country_code = 
    array(
          array(

          "id"=>"194"
          ,"name"=>"Abkhazia"
          ,"currency"=>"Ruble"
          ,"code"=>"995"
          ,"iso"=>"GE"
          ,"iso2"=>"GEO"
          ,"c"=>"268"
          ,"ext"=>".ge"
          ,"new-currency"=>"RUB"
          
          )
          ,array(

          "id"=>"1"
          ,"name"=>"Afghanistan"
          ,"currency"=>"Afghani"
          ,"code"=>"93"
          ,"iso"=>"AF"
          ,"iso2"=>"AFG"
          ,"c"=>"4"
          ,"ext"=>".af"
          ,"new-currency"=>"AFN"
          
          )
          ,array(

          "id"=>"258"
          ,"name"=>"Aland"
          ,"currency"=>"Euro"
          ,"code"=>"340"
          ,"iso"=>"AX"
          ,"iso2"=>"ALA"
          ,"c"=>"248"
          ,"ext"=>".ax"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"2"
          ,"name"=>"Albania"
          ,"currency"=>"Lek"
          ,"code"=>"355"
          ,"iso"=>"AL"
          ,"iso2"=>"ALB"
          ,"c"=>"8"
          ,"ext"=>".al"
          ,"new-currency"=>"ALL"
          
          )
          ,array(

          "id"=>"3"
          ,"name"=>"Algeria"
          ,"currency"=>"Dinar"
          ,"code"=>"213"
          ,"iso"=>"DZ"
          ,"iso2"=>"DZA"
          ,"c"=>"12"
          ,"ext"=>".dz"
          ,"new-currency"=>"DZD"
          
          )
          ,array(

          "id"=>"238"
          ,"name"=>"American Samoa"
          ,"currency"=>"Dollar"
          ,"code"=>"-683"
          ,"iso"=>"AS"
          ,"iso2"=>"ASM"
          ,"c"=>"16"
          ,"ext"=>".as"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"4"
          ,"name"=>"Andorra"
          ,"currency"=>"Euro"
          ,"code"=>"376"
          ,"iso"=>"AD"
          ,"iso2"=>"AND"
          ,"c"=>"20"
          ,"ext"=>".ad"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"5"
          ,"name"=>"Angola"
          ,"currency"=>"Kwanza"
          ,"code"=>"244"
          ,"iso"=>"AO"
          ,"iso2"=>"AGO"
          ,"c"=>"24"
          ,"ext"=>".ao"
          ,"new-currency"=>"AOA"
          
          )
          ,array(

          "id"=>"223"
          ,"name"=>"Anguilla"
          ,"currency"=>"Dollar"
          ,"code"=>"-263"
          ,"iso"=>"AI"
          ,"iso2"=>"AIA"
          ,"c"=>"660"
          ,"ext"=>".ai"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"6"
          ,"name"=>"Antigua and Barbuda"
          ,"currency"=>"Dollar"
          ,"code"=>"-267"
          ,"iso"=>"AG"
          ,"iso2"=>"ATG"
          ,"c"=>"28"
          ,"ext"=>".ag"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"7"
          ,"name"=>"Argentina"
          ,"currency"=>"Peso"
          ,"code"=>"54"
          ,"iso"=>"AR"
          ,"iso2"=>"ARG"
          ,"c"=>"32"
          ,"ext"=>".ar"
          ,"new-currency"=>"ARS"
          
          )
          ,array(

          "id"=>"8"
          ,"name"=>"Armenia"
          ,"currency"=>"Dram"
          ,"code"=>"374"
          ,"iso"=>"AM"
          ,"iso2"=>"ARM"
          ,"c"=>"51"
          ,"ext"=>".am"
          ,"new-currency"=>"AMD"
          
          )
          ,array(

          "id"=>"259"
          ,"name"=>"Aruba"
          ,"currency"=>"Guilder"
          ,"code"=>"297"
          ,"iso"=>"AW"
          ,"iso2"=>"ABW"
          ,"c"=>"533"
          ,"ext"=>".aw"
          ,"new-currency"=>"AWG"
          
          )
          ,array(

          "id"=>"9"
          ,"name"=>"Australia"
          ,"currency"=>"Dollar"
          ,"code"=>"61"
          ,"iso"=>"AU"
          ,"iso2"=>"AUS"
          ,"c"=>"36"
          ,"ext"=>".au"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"10"
          ,"name"=>"Austria"
          ,"currency"=>"Euro"
          ,"code"=>"43"
          ,"iso"=>"AT"
          ,"iso2"=>"AUT"
          ,"c"=>"40"
          ,"ext"=>".at"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"11"
          ,"name"=>"Azerbaijan"
          ,"currency"=>"Manat"
          ,"code"=>"994"
          ,"iso"=>"AZ"
          ,"iso2"=>"AZE"
          ,"c"=>"31"
          ,"ext"=>".az"
          ,"new-currency"=>"AZN"
          
          )
          ,array(

          "id"=>"12"
          ,"name"=>"Bahamas"
          ,"currency"=>" The"
          ,"code"=>"Dollar"
          ,"iso"=>"-241"
          ,"iso2"=>"BS"
          ,"c"=>"BHS"
          ,"ext"=>"44"
          ,"new-currency"=>"BSD"
          
          )
          ,array(

          "id"=>"13"
          ,"name"=>"Bahrain"
          ,"currency"=>"Dinar"
          ,"code"=>"973"
          ,"iso"=>"BH"
          ,"iso2"=>"BHR"
          ,"c"=>"48"
          ,"ext"=>".bh"
          ,"new-currency"=>"BHD"
          
          )
          ,array(

          "id"=>"14"
          ,"name"=>"Bangladesh"
          ,"currency"=>"Taka"
          ,"code"=>"880"
          ,"iso"=>"BD"
          ,"iso2"=>"BGD"
          ,"c"=>"50"
          ,"ext"=>".bd"
          ,"new-currency"=>"BDT"
          
          )
          ,array(

          "id"=>"15"
          ,"name"=>"Barbados"
          ,"currency"=>"Dollar"
          ,"code"=>"-245"
          ,"iso"=>"BB"
          ,"iso2"=>"BRB"
          ,"c"=>"52"
          ,"ext"=>".bb"
          ,"new-currency"=>"BBD"
          
          )
          ,array(

          "id"=>"16"
          ,"name"=>"Belarus"
          ,"currency"=>"Ruble"
          ,"code"=>"375"
          ,"iso"=>"BY"
          ,"iso2"=>"BLR"
          ,"c"=>"112"
          ,"ext"=>".by"
          ,"new-currency"=>"BYR"
          
          )
          ,array(

          "id"=>"17"
          ,"name"=>"Belgium"
          ,"currency"=>"Euro"
          ,"code"=>"32"
          ,"iso"=>"BE"
          ,"iso2"=>"BEL"
          ,"c"=>"56"
          ,"ext"=>".be"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"18"
          ,"name"=>"Belize"
          ,"currency"=>"Dollar"
          ,"code"=>"501"
          ,"iso"=>"BZ"
          ,"iso2"=>"BLZ"
          ,"c"=>"84"
          ,"ext"=>".bz"
          ,"new-currency"=>"BZD"
          
          )
          ,array(

          "id"=>"19"
          ,"name"=>"Benin"
          ,"currency"=>"Franc"
          ,"code"=>"229"
          ,"iso"=>"BJ"
          ,"iso2"=>"BEN"
          ,"c"=>"204"
          ,"ext"=>".bj"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"224"
          ,"name"=>"Bermuda"
          ,"currency"=>"Dollar"
          ,"code"=>"-440"
          ,"iso"=>"BM"
          ,"iso2"=>"BMU"
          ,"c"=>"60"
          ,"ext"=>".bm"
          ,"new-currency"=>"BMD"
          
          )
          ,array(

          "id"=>"20"
          ,"name"=>"Bhutan"
          ,"currency"=>"Ngultrum"
          ,"code"=>"975"
          ,"iso"=>"BT"
          ,"iso2"=>"BTN"
          ,"c"=>"64"
          ,"ext"=>".bt"
          ,"new-currency"=>"BTN"
          
          )
          ,array(

          "id"=>"21"
          ,"name"=>"Bolivia"
          ,"currency"=>"Boliviano"
          ,"code"=>"591"
          ,"iso"=>"BO"
          ,"iso2"=>"BOL"
          ,"c"=>"68"
          ,"ext"=>".bo"
          ,"new-currency"=>"BOB"
          
          )
          ,array(

          "id"=>"22"
          ,"name"=>"Bosnia and Herzegovina"
          ,"currency"=>"Marka"
          ,"code"=>"387"
          ,"iso"=>"BA"
          ,"iso2"=>"BIH"
          ,"c"=>"70"
          ,"ext"=>".ba"
          ,"new-currency"=>"BAM"
          
          )
          ,array(

          "id"=>"23"
          ,"name"=>"Botswana"
          ,"currency"=>"Pula"
          ,"code"=>"267"
          ,"iso"=>"BW"
          ,"iso2"=>"BWA"
          ,"c"=>"72"
          ,"ext"=>".bw"
          ,"new-currency"=>"BWP"
          
          )
          ,array(

          "id"=>"24"
          ,"name"=>"Brazil"
          ,"currency"=>"Real"
          ,"code"=>"55"
          ,"iso"=>"BR"
          ,"iso2"=>"BRA"
          ,"c"=>"76"
          ,"ext"=>".br"
          ,"new-currency"=>"BRL"
          
          )
          ,array(

          "id"=>"227"
          ,"name"=>"British Virgin Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"-283"
          ,"iso"=>"VG"
          ,"iso2"=>"VGB"
          ,"c"=>"92"
          ,"ext"=>".vg"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"25"
          ,"name"=>"Brunei"
          ,"currency"=>"Dollar"
          ,"code"=>"673"
          ,"iso"=>"BN"
          ,"iso2"=>"BRN"
          ,"c"=>"96"
          ,"ext"=>".bn"
          ,"new-currency"=>"BND"
          
          )
          ,array(

          "id"=>"26"
          ,"name"=>"Bulgaria"
          ,"currency"=>"Lev"
          ,"code"=>"359"
          ,"iso"=>"BG"
          ,"iso2"=>"BGR"
          ,"c"=>"100"
          ,"ext"=>".bg"
          ,"new-currency"=>"BGN"
          
          )
          ,array(

          "id"=>"27"
          ,"name"=>"Burkina Faso"
          ,"currency"=>"Franc"
          ,"code"=>"226"
          ,"iso"=>"BF"
          ,"iso2"=>"BFA"
          ,"c"=>"854"
          ,"ext"=>".bf"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"28"
          ,"name"=>"Burundi"
          ,"currency"=>"Franc"
          ,"code"=>"257"
          ,"iso"=>"BI"
          ,"iso2"=>"BDI"
          ,"c"=>"108"
          ,"ext"=>".bi"
          ,"new-currency"=>"BIF"
          
          )
          ,array(

          "id"=>"29"
          ,"name"=>"Cambodia"
          ,"currency"=>"Riels"
          ,"code"=>"855"
          ,"iso"=>"KH"
          ,"iso2"=>"KHM"
          ,"c"=>"116"
          ,"ext"=>".kh"
          ,"new-currency"=>"KHR"
          
          )
          ,array(

          "id"=>"30"
          ,"name"=>"Cameroon"
          ,"currency"=>"Franc"
          ,"code"=>"237"
          ,"iso"=>"CM"
          ,"iso2"=>"CMR"
          ,"c"=>"120"
          ,"ext"=>".cm"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"31"
          ,"name"=>"Canada"
          ,"currency"=>"Dollar"
          ,"code"=>"1"
          ,"iso"=>"CA"
          ,"iso2"=>"CAN"
          ,"c"=>"124"
          ,"ext"=>".ca"
          ,"new-currency"=>"CAD"
          
          )
          ,array(

          "id"=>"32"
          ,"name"=>"Cape Verde"
          ,"currency"=>"Escudo"
          ,"code"=>"238"
          ,"iso"=>"CV"
          ,"iso2"=>"CPV"
          ,"c"=>"132"
          ,"ext"=>".cv"
          ,"new-currency"=>"CVE"
          
          )
          ,array(

          "id"=>"228"
          ,"name"=>"Cayman Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"-344"
          ,"iso"=>"KY"
          ,"iso2"=>"CYM"
          ,"c"=>"136"
          ,"ext"=>".ky"
          ,"new-currency"=>"KYD"
          
          )
          ,array(

          "id"=>"33"
          ,"name"=>"Central African Republic"
          ,"currency"=>"Franc"
          ,"code"=>"236"
          ,"iso"=>"CF"
          ,"iso2"=>"CAF"
          ,"c"=>"140"
          ,"ext"=>".cf"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"34"
          ,"name"=>"Chad"
          ,"currency"=>"Franc"
          ,"code"=>"235"
          ,"iso"=>"TD"
          ,"iso2"=>"TCD"
          ,"c"=>"148"
          ,"ext"=>".td"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"35"
          ,"name"=>"Chile"
          ,"currency"=>"Peso"
          ,"code"=>"56"
          ,"iso"=>"CL"
          ,"iso2"=>"CHL"
          ,"c"=>"152"
          ,"ext"=>".cl"
          ,"new-currency"=>"CLP"
          
          )
          ,array(

          "id"=>"36"
          ,"name"=>"China"
          ,"currency"=>" Peoples Republic of"
          ,"code"=>"Yuan Renminbi"
          ,"iso"=>"86"
          ,"iso2"=>"CN"
          ,"c"=>"CHN"
          ,"ext"=>"156"
          ,"new-currency"=>"CNY"
          
          )
          ,array(

          "id"=>"195"
          ,"name"=>"China"
          ,"currency"=>" Republic of (Taiwan)"
          ,"code"=>"Dollar"
          ,"iso"=>"886"
          ,"iso2"=>"TW"
          ,"c"=>"TWN"
          ,"ext"=>"158"
          ,"new-currency"=>"CNY"
          
          )
          ,array(

          "id"=>"202"
          ,"name"=>"Christmas Island"
          ,"currency"=>"Dollar"
          ,"code"=>"61"
          ,"iso"=>"CX"
          ,"iso2"=>"CXR"
          ,"c"=>"162"
          ,"ext"=>".cx"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"203"
          ,"name"=>"Cocos (Keeling) Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"61"
          ,"iso"=>"CC"
          ,"iso2"=>"CCK"
          ,"c"=>"166"
          ,"ext"=>".cc"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"37"
          ,"name"=>"Colombia"
          ,"currency"=>"Peso"
          ,"code"=>"57"
          ,"iso"=>"CO"
          ,"iso2"=>"COL"
          ,"c"=>"170"
          ,"ext"=>".co"
          ,"new-currency"=>"COP"
          
          )
          ,array(

          "id"=>"38"
          ,"name"=>"Comoros"
          ,"currency"=>"Franc"
          ,"code"=>"269"
          ,"iso"=>"KM"
          ,"iso2"=>"COM"
          ,"c"=>"174"
          ,"ext"=>".km"
          ,"new-currency"=>"KMF"
          
          )
          ,array(

          "id"=>"39"
          ,"name"=>"Congo"
          ,"currency"=>" (Congo Â– Kinshasa)"
          ,"code"=>"Franc"
          ,"iso"=>"243"
          ,"iso2"=>"CD"
          ,"c"=>"COD"
          ,"ext"=>"180"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"40"
          ,"name"=>"Congo"
          ,"currency"=>" (Congo Â– Brazzaville)"
          ,"code"=>"Franc"
          ,"iso"=>"242"
          ,"iso2"=>"CG"
          ,"c"=>"COG"
          ,"ext"=>"178"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"217"
          ,"name"=>"Cook Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"682"
          ,"iso"=>"CK"
          ,"iso2"=>"COK"
          ,"c"=>"184"
          ,"ext"=>".ck"
          ,"new-currency"=>"NZD"
          
          )
          ,array(

          "id"=>"41"
          ,"name"=>"Costa Rica"
          ,"currency"=>"Colon"
          ,"code"=>"506"
          ,"iso"=>"CR"
          ,"iso2"=>"CRI"
          ,"c"=>"188"
          ,"ext"=>".cr"
          ,"new-currency"=>"CRC"
          
          )
          ,array(

          "id"=>"42"
          ,"name"=>"Cote dIvoire (Ivory Coast)"
          ,"currency"=>"Franc"
          ,"code"=>"225"
          ,"iso"=>"CI"
          ,"iso2"=>"CIV"
          ,"c"=>"384"
          ,"ext"=>".ci"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"43"
          ,"name"=>"Croatia"
          ,"currency"=>"Kuna"
          ,"code"=>"385"
          ,"iso"=>"HR"
          ,"iso2"=>"HRV"
          ,"c"=>"191"
          ,"ext"=>".hr"
          ,"new-currency"=>"HRK"
          
          )
          ,array(

          "id"=>"44"
          ,"name"=>"Cuba"
          ,"currency"=>"Peso"
          ,"code"=>"53"
          ,"iso"=>"CU"
          ,"iso2"=>"CUB"
          ,"c"=>"192"
          ,"ext"=>".cu"
          ,"new-currency"=>"CUP"
          
          )
          ,array(

          "id"=>"45"
          ,"name"=>"Cyprus"
          ,"currency"=>"Pound"
          ,"code"=>"357"
          ,"iso"=>"CY"
          ,"iso2"=>"CYP"
          ,"c"=>"196"
          ,"ext"=>".cy"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"46"
          ,"name"=>"Czech Republic"
          ,"currency"=>"Koruna"
          ,"code"=>"420"
          ,"iso"=>"CZ"
          ,"iso2"=>"CZE"
          ,"c"=>"203"
          ,"ext"=>".cz"
          ,"new-currency"=>"CZK"
          
          )
          ,array(

          "id"=>"47"
          ,"name"=>"Denmark"
          ,"currency"=>"Krone"
          ,"code"=>"45"
          ,"iso"=>"DK"
          ,"iso2"=>"DNK"
          ,"c"=>"208"
          ,"ext"=>".dk"
          ,"new-currency"=>"DKK"
          
          )
          ,array(

          "id"=>"48"
          ,"name"=>"Djibouti"
          ,"currency"=>"Franc"
          ,"code"=>"253"
          ,"iso"=>"DJ"
          ,"iso2"=>"DJI"
          ,"c"=>"262"
          ,"ext"=>".dj"
          ,"new-currency"=>"DJF"
          
          )
          ,array(

          "id"=>"49"
          ,"name"=>"Dominica"
          ,"currency"=>"Dollar"
          ,"code"=>"-766"
          ,"iso"=>"DM"
          ,"iso2"=>"DMA"
          ,"c"=>"212"
          ,"ext"=>".dm"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"50"
          ,"name"=>"Dominican Republic"
          ,"currency"=>"Peso"
          ,"code"=>"+1-809 and 1-829"
          ,"iso"=>"DO"
          ,"iso2"=>"DOM"
          ,"c"=>"214"
          ,"ext"=>".do"
          ,"new-currency"=>"DOP"
          
          )
          ,array(

          "id"=>"51"
          ,"name"=>"Ecuador"
          ,"currency"=>"Dollar"
          ,"code"=>"593"
          ,"iso"=>"EC"
          ,"iso2"=>"ECU"
          ,"c"=>"218"
          ,"ext"=>".ec"
          ,"new-currency"=>"ECS"
          
          )
          ,array(

          "id"=>"52"
          ,"name"=>"Egypt"
          ,"currency"=>"Pound"
          ,"code"=>"20"
          ,"iso"=>"EG"
          ,"iso2"=>"EGY"
          ,"c"=>"818"
          ,"ext"=>".eg"
          ,"new-currency"=>"EGP"
          
          )
          ,array(

          "id"=>"53"
          ,"name"=>"El Salvador"
          ,"currency"=>"Dollar"
          ,"code"=>"503"
          ,"iso"=>"SV"
          ,"iso2"=>"SLV"
          ,"c"=>"222"
          ,"ext"=>".sv"
          ,"new-currency"=>"SVC"
          
          )
          ,array(

          "id"=>"54"
          ,"name"=>"Equatorial Guinea"
          ,"currency"=>"Franc"
          ,"code"=>"240"
          ,"iso"=>"GQ"
          ,"iso2"=>"GNQ"
          ,"c"=>"226"
          ,"ext"=>".gq"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"55"
          ,"name"=>"Eritrea"
          ,"currency"=>"Nakfa"
          ,"code"=>"291"
          ,"iso"=>"ER"
          ,"iso2"=>"ERI"
          ,"c"=>"232"
          ,"ext"=>".er"
          ,"new-currency"=>"ERN"
          
          )
          ,array(

          "id"=>"56"
          ,"name"=>"Estonia"
          ,"currency"=>"Kroon"
          ,"code"=>"372"
          ,"iso"=>"EE"
          ,"iso2"=>"EST"
          ,"c"=>"233"
          ,"ext"=>".ee"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"57"
          ,"name"=>"Ethiopia"
          ,"currency"=>"Birr"
          ,"code"=>"251"
          ,"iso"=>"ET"
          ,"iso2"=>"ETH"
          ,"c"=>"231"
          ,"ext"=>".et"
          ,"new-currency"=>"ETB"
          
          )
          ,array(

          "id"=>"229"
          ,"name"=>"Falkland Islands (Islas Malvinas)"
          ,"currency"=>"Pound"
          ,"code"=>"500"
          ,"iso"=>"FK"
          ,"iso2"=>"FLK"
          ,"c"=>"238"
          ,"ext"=>".fk"
          ,"new-currency"=>"FKP"
          
          )
          ,array(

          "id"=>"252"
          ,"name"=>"Faroe Islands"
          ,"currency"=>"Krone"
          ,"code"=>"298"
          ,"iso"=>"FO"
          ,"iso2"=>"FRO"
          ,"c"=>"234"
          ,"ext"=>".fo"
          ,"new-currency"=>"DKK"
          
          )
          ,array(

          "id"=>"58"
          ,"name"=>"Fiji"
          ,"currency"=>"Dollar"
          ,"code"=>"679"
          ,"iso"=>"FJ"
          ,"iso2"=>"FJI"
          ,"c"=>"242"
          ,"ext"=>".fj"
          ,"new-currency"=>"FJD"
          
          )
          ,array(

          "id"=>"59"
          ,"name"=>"Finland"
          ,"currency"=>"Euro"
          ,"code"=>"358"
          ,"iso"=>"FI"
          ,"iso2"=>"FIN"
          ,"c"=>"246"
          ,"ext"=>".fi"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"60"
          ,"name"=>"France"
          ,"currency"=>"Euro"
          ,"code"=>"33"
          ,"iso"=>"FR"
          ,"iso2"=>"FRA"
          ,"c"=>"250"
          ,"ext"=>".fr"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"254"
          ,"name"=>"French Guiana"
          ,"currency"=>"Euro"
          ,"code"=>"594"
          ,"iso"=>"GF"
          ,"iso2"=>"GUF"
          ,"c"=>"254"
          ,"ext"=>".gf"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"208"
          ,"name"=>"French Polynesia"
          ,"currency"=>"Franc"
          ,"code"=>"689"
          ,"iso"=>"PF"
          ,"iso2"=>"PYF"
          ,"c"=>"258"
          ,"ext"=>".pf"
          ,"new-currency"=>"CFP"
          
          )
          ,array(

          "id"=>"61"
          ,"name"=>"Gabon"
          ,"currency"=>"Franc"
          ,"code"=>"241"
          ,"iso"=>"GA"
          ,"iso2"=>"GAB"
          ,"c"=>"266"
          ,"ext"=>".ga"
          ,"new-currency"=>"XAF"
          
          )
          ,array(

          "id"=>"62"
          ,"name"=>"Gambia"
          ,"currency"=>" The"
          ,"code"=>"Dalasi"
          ,"iso"=>"220"
          ,"iso2"=>"GM"
          ,"c"=>"GMB"
          ,"ext"=>"270"
          ,"new-currency"=>"GMD"
          
          )
          ,array(

          "id"=>"63"
          ,"name"=>"Georgia"
          ,"currency"=>"Lari"
          ,"code"=>"995"
          ,"iso"=>"GE"
          ,"iso2"=>"GEO"
          ,"c"=>"268"
          ,"ext"=>".ge"
          ,"new-currency"=>"GEL"
          
          )
          ,array(

          "id"=>"64"
          ,"name"=>"Germany"
          ,"currency"=>"Euro"
          ,"code"=>"49"
          ,"iso"=>"DE"
          ,"iso2"=>"DEU"
          ,"c"=>"276"
          ,"ext"=>".de"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"65"
          ,"name"=>"Ghana"
          ,"currency"=>"Cedi"
          ,"code"=>"233"
          ,"iso"=>"GH"
          ,"iso2"=>"GHA"
          ,"c"=>"288"
          ,"ext"=>".gh"
          ,"new-currency"=>"GHS"
          
          )
          ,array(

          "id"=>"230"
          ,"name"=>"Gibraltar"
          ,"currency"=>"Pound"
          ,"code"=>"350"
          ,"iso"=>"GI"
          ,"iso2"=>"GIB"
          ,"c"=>"292"
          ,"ext"=>".gi"
          ,"new-currency"=>"GIP"
          
          )
          ,array(

          "id"=>"66"
          ,"name"=>"Greece"
          ,"currency"=>"Euro"
          ,"code"=>"30"
          ,"iso"=>"GR"
          ,"iso2"=>"GRC"
          ,"c"=>"300"
          ,"ext"=>".gr"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"253"
          ,"name"=>"Greenland"
          ,"currency"=>"Krone"
          ,"code"=>"299"
          ,"iso"=>"GL"
          ,"iso2"=>"GRL"
          ,"c"=>"304"
          ,"ext"=>".gl"
          ,"new-currency"=>"DKK"
          
          )
          ,array(

          "id"=>"67"
          ,"name"=>"Grenada"
          ,"currency"=>"Dollar"
          ,"code"=>"-472"
          ,"iso"=>"GD"
          ,"iso2"=>"GRD"
          ,"c"=>"308"
          ,"ext"=>".gd"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"255"
          ,"name"=>"Guadeloupe"
          ,"currency"=>"Euro"
          ,"code"=>"590"
          ,"iso"=>"GP"
          ,"iso2"=>"GLP"
          ,"c"=>"312"
          ,"ext"=>".gp"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"240"
          ,"name"=>"Guam"
          ,"currency"=>"Dollar"
          ,"code"=>"-670"
          ,"iso"=>"GU"
          ,"iso2"=>"GUM"
          ,"c"=>"316"
          ,"ext"=>".gu"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"68"
          ,"name"=>"Guatemala"
          ,"currency"=>"Quetzal"
          ,"code"=>"502"
          ,"iso"=>"GT"
          ,"iso2"=>"GTM"
          ,"c"=>"320"
          ,"ext"=>".gt"
          ,"new-currency"=>"QTQ"
          
          )
          ,array(

          "id"=>"220"
          ,"name"=>"Guernsey"
          ,"currency"=>"Pound"
          ,"code"=>"44"
          ,"iso"=>"GG"
          ,"iso2"=>"GGY"
          ,"c"=>"831"
          ,"ext"=>".gg"
          ,"new-currency"=>"GGP"
          
          )
          ,array(

          "id"=>"69"
          ,"name"=>"Guinea"
          ,"currency"=>"Franc"
          ,"code"=>"224"
          ,"iso"=>"GN"
          ,"iso2"=>"GIN"
          ,"c"=>"324"
          ,"ext"=>".gn"
          ,"new-currency"=>"GNF"
          
          )
          ,array(

          "id"=>"70"
          ,"name"=>"Guinea-Bissau"
          ,"currency"=>"Franc"
          ,"code"=>"245"
          ,"iso"=>"GW"
          ,"iso2"=>"GNB"
          ,"c"=>"624"
          ,"ext"=>".gw"
          ,"new-currency"=>"GWP"
          
          )
          ,array(

          "id"=>"71"
          ,"name"=>"Guyana"
          ,"currency"=>"Dollar"
          ,"code"=>"592"
          ,"iso"=>"GY"
          ,"iso2"=>"GUY"
          ,"c"=>"328"
          ,"ext"=>".gy"
          ,"new-currency"=>"GYD"
          
          )
          ,array(

          "id"=>"72"
          ,"name"=>"Haiti"
          ,"currency"=>"Gourde"
          ,"code"=>"509"
          ,"iso"=>"HT"
          ,"iso2"=>"HTI"
          ,"c"=>"332"
          ,"ext"=>".ht"
          ,"new-currency"=>"HTG"
          
          )
          ,array(

          "id"=>"73"
          ,"name"=>"Honduras"
          ,"currency"=>"Lempira"
          ,"code"=>"504"
          ,"iso"=>"HN"
          ,"iso2"=>"HND"
          ,"c"=>"340"
          ,"ext"=>".hn"
          ,"new-currency"=>"HNL"
          
          )
          ,array(

          "id"=>"250"
          ,"name"=>"Hong Kong"
          ,"currency"=>"Dollar"
          ,"code"=>"852"
          ,"iso"=>"HK"
          ,"iso2"=>"HKG"
          ,"c"=>"344"
          ,"ext"=>".hk"
          ,"new-currency"=>"HKD"
          
          )
          ,array(

          "id"=>"74"
          ,"name"=>"Hungary"
          ,"currency"=>"Forint"
          ,"code"=>"36"
          ,"iso"=>"HU"
          ,"iso2"=>"HUN"
          ,"c"=>"348"
          ,"ext"=>".hu"
          ,"new-currency"=>"HUF"
          
          )
          ,array(

          "id"=>"75"
          ,"name"=>"Iceland"
          ,"currency"=>"Krona"
          ,"code"=>"354"
          ,"iso"=>"IS"
          ,"iso2"=>"ISL"
          ,"c"=>"352"
          ,"ext"=>".is"
          ,"new-currency"=>"ISK"
          
          )
          ,array(

          "id"=>"76"
          ,"name"=>"India"
          ,"currency"=>"Rupee"
          ,"code"=>"91"
          ,"iso"=>"IN"
          ,"iso2"=>"IND"
          ,"c"=>"356"
          ,"ext"=>".in"
          ,"new-currency"=>"INR"
          
          )
          ,array(

          "id"=>"77"
          ,"name"=>"Indonesia"
          ,"currency"=>"Rupiah"
          ,"code"=>"62"
          ,"iso"=>"ID"
          ,"iso2"=>"IDN"
          ,"c"=>"360"
          ,"ext"=>".id"
          ,"new-currency"=>"IDR"
          
          )
          ,array(

          "id"=>"78"
          ,"name"=>"Iran"
          ,"currency"=>"Rial"
          ,"code"=>"98"
          ,"iso"=>"IR"
          ,"iso2"=>"IRN"
          ,"c"=>"364"
          ,"ext"=>".ir"
          ,"new-currency"=>"IRR"
          
          )
          ,array(

          "id"=>"79"
          ,"name"=>"Iraq"
          ,"currency"=>"Dinar"
          ,"code"=>"964"
          ,"iso"=>"IQ"
          ,"iso2"=>"IRQ"
          ,"c"=>"368"
          ,"ext"=>".iq"
          ,"new-currency"=>"IQD"
          
          )
          ,array(

          "id"=>"80"
          ,"name"=>"Ireland"
          ,"currency"=>"Euro"
          ,"code"=>"353"
          ,"iso"=>"IE"
          ,"iso2"=>"IRL"
          ,"c"=>"372"
          ,"ext"=>".ie"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"221"
          ,"name"=>"Isle of Man"
          ,"currency"=>"Pound"
          ,"code"=>"44"
          ,"iso"=>"IM"
          ,"iso2"=>"IMN"
          ,"c"=>"833"
          ,"ext"=>".im"
          ,"new-currency"=>"GBP"
          
          )
          ,array(

          "id"=>"81"
          ,"name"=>"Israel"
          ,"currency"=>"Shekel"
          ,"code"=>"972"
          ,"iso"=>"IL"
          ,"iso2"=>"ISR"
          ,"c"=>"376"
          ,"ext"=>".il"
          ,"new-currency"=>"ILS"
          
          )
          ,array(

          "id"=>"82"
          ,"name"=>"Italy"
          ,"currency"=>"Euro"
          ,"code"=>"39"
          ,"iso"=>"IT"
          ,"iso2"=>"ITA"
          ,"c"=>"380"
          ,"ext"=>".it"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"83"
          ,"name"=>"Jamaica"
          ,"currency"=>"Dollar"
          ,"code"=>"-875"
          ,"iso"=>"JM"
          ,"iso2"=>"JAM"
          ,"c"=>"388"
          ,"ext"=>".jm"
          ,"new-currency"=>"JMD"
          
          )
          ,array(

          "id"=>"84"
          ,"name"=>"Japan"
          ,"currency"=>"Yen"
          ,"code"=>"81"
          ,"iso"=>"JP"
          ,"iso2"=>"JPN"
          ,"c"=>"392"
          ,"ext"=>".jp"
          ,"new-currency"=>"JPY"
          
          )
          ,array(

          "id"=>"222"
          ,"name"=>"Jersey"
          ,"currency"=>"Pound"
          ,"code"=>"44"
          ,"iso"=>"JE"
          ,"iso2"=>"JEY"
          ,"c"=>"832"
          ,"ext"=>".je"
          ,"new-currency"=>"GBP"
          
          )
          ,array(

          "id"=>"85"
          ,"name"=>"Jordan"
          ,"currency"=>"Dinar"
          ,"code"=>"962"
          ,"iso"=>"JO"
          ,"iso2"=>"JOR"
          ,"c"=>"400"
          ,"ext"=>".jo"
          ,"new-currency"=>"JOD"
          
          )
          ,array(

          "id"=>"86"
          ,"name"=>"Kazakhstan"
          ,"currency"=>"Tenge"
          ,"code"=>"7"
          ,"iso"=>"KZ"
          ,"iso2"=>"KAZ"
          ,"c"=>"398"
          ,"ext"=>".kz"
          ,"new-currency"=>"KZT"
          
          )
          ,array(

          "id"=>"87"
          ,"name"=>"Kenya"
          ,"currency"=>"Shilling"
          ,"code"=>"254"
          ,"iso"=>"KE"
          ,"iso2"=>"KEN"
          ,"c"=>"404"
          ,"ext"=>".ke"
          ,"new-currency"=>"KES"
          
          )
          ,array(

          "id"=>"88"
          ,"name"=>"Kiribati"
          ,"currency"=>"Dollar"
          ,"code"=>"686"
          ,"iso"=>"KI"
          ,"iso2"=>"KIR"
          ,"c"=>"296"
          ,"ext"=>".ki"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"89"
          ,"name"=>"North Korea"//NORTH
          ,"currency"=>"WON"
          ,"code"=>"Won"
          ,"iso"=>"850"
          ,"iso2"=>"KP"
          ,"c"=>"PRK"
          ,"ext"=>"408"
          ,"new-currency"=>"KPW"
          
          )
          ,array(

          "id"=>"90"
          ,"name"=>"South Korea"//SOUTH
          ,"currency"=>"WON"
          ,"code"=>"Won"
          ,"iso"=>"82"
          ,"iso2"=>"KR"
          ,"c"=>"KOR"
          ,"ext"=>"410"
          ,"new-currency"=>"KRW"
          
          )
          ,array(

          "id"=>"91"
          ,"name"=>"Kuwait"
          ,"currency"=>"Dinar"
          ,"code"=>"965"
          ,"iso"=>"KW"
          ,"iso2"=>"KWT"
          ,"c"=>"414"
          ,"ext"=>".kw"
          ,"new-currency"=>"KWD"
          
          )
          ,array(

          "id"=>"92"
          ,"name"=>"Kyrgyzstan"
          ,"currency"=>"Som"
          ,"code"=>"996"
          ,"iso"=>"KG"
          ,"iso2"=>"KGZ"
          ,"c"=>"417"
          ,"ext"=>".kg"
          ,"new-currency"=>"KGS"
          
          )
          ,array(

          "id"=>"93"
          ,"name"=>"Laos"
          ,"currency"=>"Kip"
          ,"code"=>"856"
          ,"iso"=>"LA"
          ,"iso2"=>"LAO"
          ,"c"=>"418"
          ,"ext"=>".la"
          ,"new-currency"=>"LAK"
          
          )
          ,array(

          "id"=>"94"
          ,"name"=>"Latvia"
          ,"currency"=>"Lat"
          ,"code"=>"371"
          ,"iso"=>"LV"
          ,"iso2"=>"LVA"
          ,"c"=>"428"
          ,"ext"=>".lv"
          ,"new-currency"=>"LVL"
          
          )
          ,array(

          "id"=>"95"
          ,"name"=>"Lebanon"
          ,"currency"=>"Pound"
          ,"code"=>"961"
          ,"iso"=>"LB"
          ,"iso2"=>"LBN"
          ,"c"=>"422"
          ,"ext"=>".lb"
          ,"new-currency"=>"LBP"
          
          )
          ,array(

          "id"=>"96"
          ,"name"=>"Lesotho"
          ,"currency"=>"Loti"
          ,"code"=>"266"
          ,"iso"=>"LS"
          ,"iso2"=>"LSO"
          ,"c"=>"426"
          ,"ext"=>".ls"
          ,"new-currency"=>"LSL"
          
          )
          ,array(

          "id"=>"97"
          ,"name"=>"Liberia"
          ,"currency"=>"Dollar"
          ,"code"=>"231"
          ,"iso"=>"LR"
          ,"iso2"=>"LBR"
          ,"c"=>"430"
          ,"ext"=>".lr"
          ,"new-currency"=>"LRD"
          
          )
          ,array(

          "id"=>"98"
          ,"name"=>"Libya"
          ,"currency"=>"Dinar"
          ,"code"=>"218"
          ,"iso"=>"LY"
          ,"iso2"=>"LBY"
          ,"c"=>"434"
          ,"ext"=>".ly"
          ,"new-currency"=>"LYD"
          
          )
          ,array(

          "id"=>"99"
          ,"name"=>"Liechtenstein"
          ,"currency"=>"Franc"
          ,"code"=>"423"
          ,"iso"=>"LI"
          ,"iso2"=>"LIE"
          ,"c"=>"438"
          ,"ext"=>".li"
          ,"new-currency"=>"CHF"
          
          )
          ,array(

          "id"=>"100"
          ,"name"=>"Lithuania"
          ,"currency"=>"Litas"
          ,"code"=>"370"
          ,"iso"=>"LT"
          ,"iso2"=>"LTU"
          ,"c"=>"440"
          ,"ext"=>".lt"
          ,"new-currency"=>"LTL"
          
          )
          ,array(

          "id"=>"101"
          ,"name"=>"Luxembourg"
          ,"currency"=>"Euro"
          ,"code"=>"352"
          ,"iso"=>"LU"
          ,"iso2"=>"LUX"
          ,"c"=>"442"
          ,"ext"=>".lu"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"251"
          ,"name"=>"Macau"
          ,"currency"=>"Pataca"
          ,"code"=>"853"
          ,"iso"=>"MO"
          ,"iso2"=>"MAC"
          ,"c"=>"446"
          ,"ext"=>".mo"
          ,"new-currency"=>"MOP"
          
          )
          ,array(

          "id"=>"102"
          ,"name"=>"Macedonia"
          ,"currency"=>"Denar"
          ,"code"=>"389"
          ,"iso"=>"MK"
          ,"iso2"=>"MKD"
          ,"c"=>"807"
          ,"ext"=>".mk"
          ,"new-currency"=>"MKD"
          
          )
          ,array(

          "id"=>"103"
          ,"name"=>"Madagascar"
          ,"currency"=>"Ariary"
          ,"code"=>"261"
          ,"iso"=>"MG"
          ,"iso2"=>"MDG"
          ,"c"=>"450"
          ,"ext"=>".mg"
          ,"new-currency"=>"MGF"
          
          )
          ,array(

          "id"=>"104"
          ,"name"=>"Malawi"
          ,"currency"=>"Kwacha"
          ,"code"=>"265"
          ,"iso"=>"MW"
          ,"iso2"=>"MWI"
          ,"c"=>"454"
          ,"ext"=>".mw"
          ,"new-currency"=>"MWK"
          
          )
          ,array(

          "id"=>"105"
          ,"name"=>"Malaysia"
          ,"currency"=>"Ringgit"
          ,"code"=>"60"
          ,"iso"=>"MY"
          ,"iso2"=>"MYS"
          ,"c"=>"458"
          ,"ext"=>".my"
          ,"new-currency"=>"MYR"
          
          )
          ,array(

          "id"=>"106"
          ,"name"=>"Maldives"
          ,"currency"=>"Rufiyaa"
          ,"code"=>"960"
          ,"iso"=>"MV"
          ,"iso2"=>"MDV"
          ,"c"=>"462"
          ,"ext"=>".mv"
          ,"new-currency"=>"MVR"
          
          )
          ,array(

          "id"=>"107"
          ,"name"=>"Mali"
          ,"currency"=>"Franc"
          ,"code"=>"223"
          ,"iso"=>"ML"
          ,"iso2"=>"MLI"
          ,"c"=>"466"
          ,"ext"=>".ml"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"108"
          ,"name"=>"Malta"
          ,"currency"=>"Lira"
          ,"code"=>"356"
          ,"iso"=>"MT"
          ,"iso2"=>"MLT"
          ,"c"=>"470"
          ,"ext"=>".mt"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"109"
          ,"name"=>"Marshall Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"692"
          ,"iso"=>"MH"
          ,"iso2"=>"MHL"
          ,"c"=>"584"
          ,"ext"=>".mh"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"256"
          ,"name"=>"Martinique"
          ,"currency"=>"Euro"
          ,"code"=>"596"
          ,"iso"=>"MQ"
          ,"iso2"=>"MTQ"
          ,"c"=>"474"
          ,"ext"=>".mq"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"110"
          ,"name"=>"Mauritania"
          ,"currency"=>"Ouguiya"
          ,"code"=>"222"
          ,"iso"=>"MR"
          ,"iso2"=>"MRT"
          ,"c"=>"478"
          ,"ext"=>".mr"
          ,"new-currency"=>"MRO"
          
          )
          ,array(

          "id"=>"111"
          ,"name"=>"Mauritius"
          ,"currency"=>"Rupee"
          ,"code"=>"230"
          ,"iso"=>"MU"
          ,"iso2"=>"MUS"
          ,"c"=>"480"
          ,"ext"=>".mu"
          ,"new-currency"=>"MUR"
          
          )
          ,array(

          "id"=>"209"
          ,"name"=>"Mayotte"
          ,"currency"=>"Euro"
          ,"code"=>"262"
          ,"iso"=>"YT"
          ,"iso2"=>"MYT"
          ,"c"=>"175"
          ,"ext"=>".yt"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"112"
          ,"name"=>"Mexico"
          ,"currency"=>"Peso"
          ,"code"=>"52"
          ,"iso"=>"MX"
          ,"iso2"=>"MEX"
          ,"c"=>"484"
          ,"ext"=>".mx"
          ,"new-currency"=>"MXN"
          
          )
          ,array(

          "id"=>"113"
          ,"name"=>"Micronesia"
          ,"currency"=>"Dollar"
          ,"code"=>"691"
          ,"iso"=>"FM"
          ,"iso2"=>"FSM"
          ,"c"=>"583"
          ,"ext"=>".fm"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"114"
          ,"name"=>"Moldova"
          ,"currency"=>"Leu"
          ,"code"=>"373"
          ,"iso"=>"MD"
          ,"iso2"=>"MDA"
          ,"c"=>"498"
          ,"ext"=>".md"
          ,"new-currency"=>"MDL"
          
          )
          ,array(

          "id"=>"115"
          ,"name"=>"Monaco"
          ,"currency"=>"Euro"
          ,"code"=>"377"
          ,"iso"=>"MC"
          ,"iso2"=>"MCO"
          ,"c"=>"492"
          ,"ext"=>".mc"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"116"
          ,"name"=>"Mongolia"
          ,"currency"=>"Tugrik"
          ,"code"=>"976"
          ,"iso"=>"MN"
          ,"iso2"=>"MNG"
          ,"c"=>"496"
          ,"ext"=>".mn"
          ,"new-currency"=>"MNT"
          
          )
          ,array(

          "id"=>"117"
          ,"name"=>"Montenegro"
          ,"currency"=>"Euro"
          ,"code"=>"382"
          ,"iso"=>"ME"
          ,"iso2"=>"MNE"
          ,"c"=>"499"
          ,"ext"=>".me and .yu"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"231"
          ,"name"=>"Montserrat"
          ,"currency"=>"Dollar"
          ,"code"=>"-663"
          ,"iso"=>"MS"
          ,"iso2"=>"MSR"
          ,"c"=>"500"
          ,"ext"=>".ms"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"118"
          ,"name"=>"Morocco"
          ,"currency"=>"Dirham"
          ,"code"=>"212"
          ,"iso"=>"MA"
          ,"iso2"=>"MAR"
          ,"c"=>"504"
          ,"ext"=>".ma"
          ,"new-currency"=>"MAD"
          
          )
          ,array(

          "id"=>"119"
          ,"name"=>"Mozambique"
          ,"currency"=>"Meticail"
          ,"code"=>"258"
          ,"iso"=>"MZ"
          ,"iso2"=>"MOZ"
          ,"c"=>"508"
          ,"ext"=>".mz"
          ,"new-currency"=>"MZN"
          
          )
          ,array(

          "id"=>"120"
          ,"name"=>"Myanmar (Burma)"
          ,"currency"=>"Kyat"
          ,"code"=>"95"
          ,"iso"=>"MM"
          ,"iso2"=>"MMR"
          ,"c"=>"104"
          ,"ext"=>".mm"
          ,"new-currency"=>"Kyat"
          
          )
          ,array(

          "id"=>"196"
          ,"name"=>"Nagorno-Karabakh"
          ,"currency"=>"Dram"
          ,"code"=>"277"
          ,"iso"=>"AZ"
          ,"iso2"=>"AZE"
          ,"c"=>"31"
          ,"ext"=>".az"
          ,"new-currency"=>"Dram"
          
          )
          ,array(

          "id"=>"121"
          ,"name"=>"Namibia"
          ,"currency"=>"Dollar"
          ,"code"=>"264"
          ,"iso"=>"NA"
          ,"iso2"=>"NAM"
          ,"c"=>"516"
          ,"ext"=>".na"
          ,"new-currency"=>"NAD"
          
          )
          ,array(

          "id"=>"122"
          ,"name"=>"Nauru"
          ,"currency"=>"Dollar"
          ,"code"=>"674"
          ,"iso"=>"NR"
          ,"iso2"=>"NRU"
          ,"c"=>"520"
          ,"ext"=>".nr"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"123"
          ,"name"=>"Nepal"
          ,"currency"=>"Rupee"
          ,"code"=>"977"
          ,"iso"=>"NP"
          ,"iso2"=>"NPL"
          ,"c"=>"524"
          ,"ext"=>".np"
          ,"new-currency"=>"NPR"
          
          )
          ,array(

          "id"=>"124"
          ,"name"=>"Netherlands"
          ,"currency"=>"Euro"
          ,"code"=>"31"
          ,"iso"=>"NL"
          ,"iso2"=>"NLD"
          ,"c"=>"528"
          ,"ext"=>".nl"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"260"
          ,"name"=>"Netherlands Antilles"
          ,"currency"=>"Guilder"
          ,"code"=>"599"
          ,"iso"=>"AN"
          ,"iso2"=>"ANT"
          ,"c"=>"530"
          ,"ext"=>".an"
          ,"new-currency"=>"ANG"
          
          )
          ,array(

          "id"=>"207"
          ,"name"=>"New Caledonia"
          ,"currency"=>"Franc"
          ,"code"=>"687"
          ,"iso"=>"NC"
          ,"iso2"=>"NCL"
          ,"c"=>"540"
          ,"ext"=>".nc"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"125"
          ,"name"=>"New Zealand"
          ,"currency"=>"Dollar"
          ,"code"=>"64"
          ,"iso"=>"NZ"
          ,"iso2"=>"NZL"
          ,"c"=>"554"
          ,"ext"=>".nz"
          ,"new-currency"=>"NZD"
          
          )
          ,array(

          "id"=>"126"
          ,"name"=>"Nicaragua"
          ,"currency"=>"Cordoba"
          ,"code"=>"505"
          ,"iso"=>"NI"
          ,"iso2"=>"NIC"
          ,"c"=>"558"
          ,"ext"=>".ni"
          ,"new-currency"=>"NIO"
          
          )
          ,array(

          "id"=>"127"
          ,"name"=>"Niger"
          ,"currency"=>"Franc"
          ,"code"=>"227"
          ,"iso"=>"NE"
          ,"iso2"=>"NER"
          ,"c"=>"562"
          ,"ext"=>".ne"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"128"
          ,"name"=>"Nigeria"
          ,"currency"=>"Naira"
          ,"code"=>"234"
          ,"iso"=>"NG"
          ,"iso2"=>"NGA"
          ,"c"=>"566"
          ,"ext"=>".ng"
          ,"new-currency"=>"NGN"
          
          )
          ,array(

          "id"=>"218"
          ,"name"=>"Niue"
          ,"currency"=>"Dollar"
          ,"code"=>"683"
          ,"iso"=>"NU"
          ,"iso2"=>"NIU"
          ,"c"=>"570"
          ,"ext"=>".nu"
          ,"new-currency"=>"NZD"
          
          )
          ,array(

          "id"=>"206"
          ,"name"=>"Norfolk Island"
          ,"currency"=>"Dollar"
          ,"code"=>"672"
          ,"iso"=>"NF"
          ,"iso2"=>"NFK"
          ,"c"=>"574"
          ,"ext"=>".nf"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"197"
          ,"name"=>"Northern Cyprus"
          ,"currency"=>"Lira"
          ,"code"=>"-302"
          ,"iso"=>"CY"
          ,"iso2"=>"CYP"
          ,"c"=>"196"
          ,"ext"=>".nc.tr"
          ,"new-currency"=>"Lira"
          
          )
          ,array(

          "id"=>"236"
          ,"name"=>"Northern Mariana Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"-669"
          ,"iso"=>"MP"
          ,"iso2"=>"MNP"
          ,"c"=>"580"
          ,"ext"=>".mp"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"129"
          ,"name"=>"Norway"
          ,"currency"=>"Krone"
          ,"code"=>"47"
          ,"iso"=>"NO"
          ,"iso2"=>"NOR"
          ,"c"=>"578"
          ,"ext"=>".no"
          ,"new-currency"=>"NOK"
          
          )
          ,array(

          "id"=>"130"
          ,"name"=>"Oman"
          ,"currency"=>"Rial"
          ,"code"=>"968"
          ,"iso"=>"OM"
          ,"iso2"=>"OMN"
          ,"c"=>"512"
          ,"ext"=>".om"
          ,"new-currency"=>"OMR"
          
          )
          ,array(

          "id"=>"131"
          ,"name"=>"Pakistan"
          ,"currency"=>"Rupee"
          ,"code"=>"92"
          ,"iso"=>"PK"
          ,"iso2"=>"PAK"
          ,"c"=>"586"
          ,"ext"=>".pk"
          ,"new-currency"=>"PKR"
          
          )
          ,array(

          "id"=>"132"
          ,"name"=>"Palau"
          ,"currency"=>"Dollar"
          ,"code"=>"680"
          ,"iso"=>"PW"
          ,"iso2"=>"PLW"
          ,"c"=>"585"
          ,"ext"=>".pw"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"133"
          ,"name"=>"Panama"
          ,"currency"=>"Balboa"
          ,"code"=>"507"
          ,"iso"=>"PA"
          ,"iso2"=>"PAN"
          ,"c"=>"591"
          ,"ext"=>".pa"
          ,"new-currency"=>"PAB"
          
          )
          ,array(

          "id"=>"134"
          ,"name"=>"Papua New Guinea"
          ,"currency"=>"Kina"
          ,"code"=>"675"
          ,"iso"=>"PG"
          ,"iso2"=>"PNG"
          ,"c"=>"598"
          ,"ext"=>".pg"
          ,"new-currency"=>"PGK"
          
          )
          ,array(

          "id"=>"135"
          ,"name"=>"Paraguay"
          ,"currency"=>"Guarani"
          ,"code"=>"595"
          ,"iso"=>"PY"
          ,"iso2"=>"PRY"
          ,"c"=>"600"
          ,"ext"=>".py"
          ,"new-currency"=>"PYG"
          
          )
          ,array(

          "id"=>"136"
          ,"name"=>"Peru"
          ,"currency"=>"Sol"
          ,"code"=>"51"
          ,"iso"=>"PE"
          ,"iso2"=>"PER"
          ,"c"=>"604"
          ,"ext"=>".pe"
          ,"new-currency"=>"PEN"
          
          )
          ,array(

          "id"=>"137"
          ,"name"=>"Philippines"
          ,"currency"=>"Peso"
          ,"code"=>"63"
          ,"iso"=>"PH"
          ,"iso2"=>"PHL"
          ,"c"=>"608"
          ,"ext"=>".ph"
          ,"new-currency"=>"PHP"
          
          )
          ,array(

          "id"=>"138"
          ,"name"=>"Poland"
          ,"currency"=>"Zloty"
          ,"code"=>"48"
          ,"iso"=>"PL"
          ,"iso2"=>"POL"
          ,"c"=>"616"
          ,"ext"=>".pl"
          ,"new-currency"=>"PLN"
          
          )
          ,array(

          "id"=>"139"
          ,"name"=>"Portugal"
          ,"currency"=>"Euro"
          ,"code"=>"351"
          ,"iso"=>"PT"
          ,"iso2"=>"PRT"
          ,"c"=>"620"
          ,"ext"=>".pt"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"198"
          ,"name"=>"Pridnestrovie (Transnistria)"
          ,"currency"=>"Ruple"
          ,"code"=>"-160"
          ,"iso"=>"MD"
          ,"iso2"=>"MDA"
          ,"c"=>"498"
          ,"ext"=>".md"
          ,"new-currency"=>"PRB"
          
          )
          ,array(

          "id"=>"237"
          ,"name"=>"Puerto Rico"
          ,"currency"=>"Dollar"
          ,"code"=>"+1-787 and 1-939"
          ,"iso"=>"PR"
          ,"iso2"=>"PRI"
          ,"c"=>"630"
          ,"ext"=>".pr"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"140"
          ,"name"=>"Qatar"
          ,"currency"=>"Rial"
          ,"code"=>"974"
          ,"iso"=>"QA"
          ,"iso2"=>"QAT"
          ,"c"=>"634"
          ,"ext"=>".qa"
          ,"new-currency"=>"QAR"
          
          )
          ,array(

          "id"=>"257"
          ,"name"=>"Reunion"
          ,"currency"=>"Euro"
          ,"code"=>"262"
          ,"iso"=>"RE"
          ,"iso2"=>"REU"
          ,"c"=>"638"
          ,"ext"=>".re"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"141"
          ,"name"=>"Romania"
          ,"currency"=>"Leu"
          ,"code"=>"40"
          ,"iso"=>"RO"
          ,"iso2"=>"ROU"
          ,"c"=>"642"
          ,"ext"=>".ro"
          ,"new-currency"=>"RON"
          
          )
          ,array(

          "id"=>"142"
          ,"name"=>"Russia"
          ,"currency"=>"Ruble"
          ,"code"=>"7"
          ,"iso"=>"RU"
          ,"iso2"=>"RUS"
          ,"c"=>"643"
          ,"ext"=>".ru and .su"
          ,"new-currency"=>"RUB"
          
          )
          ,array(

          "id"=>"143"
          ,"name"=>"Rwanda"
          ,"currency"=>"Franc"
          ,"code"=>"250"
          ,"iso"=>"RW"
          ,"iso2"=>"RWA"
          ,"c"=>"646"
          ,"ext"=>".rw"
          ,"new-currency"=>"RWF"
          
          )
          ,array(

          "id"=>"210"
          ,"name"=>"Saint Barthelemy"
          ,"currency"=>"Euro"
          ,"code"=>"590"
          ,"iso"=>"GP"
          ,"iso2"=>"GLP"
          ,"c"=>"312"
          ,"ext"=>".gp"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"233"
          ,"name"=>"Saint Helena"
          ,"currency"=>"Pound"
          ,"code"=>"290"
          ,"iso"=>"SH"
          ,"iso2"=>"SHN"
          ,"c"=>"654"
          ,"ext"=>".sh"
          ,"new-currency"=>"SHP"
          
          )
          ,array(

          "id"=>"144"
          ,"name"=>"Saint Kitts and Nevis"
          ,"currency"=>"Dollar"
          ,"code"=>"-868"
          ,"iso"=>"KN"
          ,"iso2"=>"KNA"
          ,"c"=>"659"
          ,"ext"=>".kn"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"145"
          ,"name"=>"Saint Lucia"
          ,"currency"=>"Dollar"
          ,"code"=>"-757"
          ,"iso"=>"LC"
          ,"iso2"=>"LCA"
          ,"c"=>"662"
          ,"ext"=>".lc"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"211"
          ,"name"=>"Saint Martin"
          ,"currency"=>"Euro"
          ,"code"=>"590"
          ,"iso"=>"GP"
          ,"iso2"=>"GLP"
          ,"c"=>"312"
          ,"ext"=>".gp"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"212"
          ,"name"=>"Saint Pierre and Miquelon"
          ,"currency"=>"Euro"
          ,"code"=>"508"
          ,"iso"=>"PM"
          ,"iso2"=>"SPM"
          ,"c"=>"666"
          ,"ext"=>".pm"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"146"
          ,"name"=>"Saint Vincent and the Grenadines"
          ,"currency"=>"Dollar"
          ,"code"=>"-783"
          ,"iso"=>"VC"
          ,"iso2"=>"VCT"
          ,"c"=>"670"
          ,"ext"=>".vc"
          ,"new-currency"=>"XCD"
          
          )
          ,array(

          "id"=>"147"
          ,"name"=>"Samoa"
          ,"currency"=>"Tala"
          ,"code"=>"685"
          ,"iso"=>"WS"
          ,"iso2"=>"WSM"
          ,"c"=>"882"
          ,"ext"=>".ws"
          ,"new-currency"=>"WST"
          
          )
          ,array(

          "id"=>"148"
          ,"name"=>"San Marino"
          ,"currency"=>"Euro"
          ,"code"=>"378"
          ,"iso"=>"SM"
          ,"iso2"=>"SMR"
          ,"c"=>"674"
          ,"ext"=>".sm"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"149"
          ,"name"=>"Sao Tome and Principe"
          ,"currency"=>"Dobra"
          ,"code"=>"239"
          ,"iso"=>"ST"
          ,"iso2"=>"STP"
          ,"c"=>"678"
          ,"ext"=>".st"
          ,"new-currency"=>"STD"
          
          )
          ,array(

          "id"=>"150"
          ,"name"=>"Saudi Arabia"
          ,"currency"=>"Rial"
          ,"code"=>"966"
          ,"iso"=>"SA"
          ,"iso2"=>"SAU"
          ,"c"=>"682"
          ,"ext"=>".sa"
          ,"new-currency"=>"SAR"
          
          )
          ,array(

          "id"=>"151"
          ,"name"=>"Senegal"
          ,"currency"=>"Franc"
          ,"code"=>"221"
          ,"iso"=>"SN"
          ,"iso2"=>"SEN"
          ,"c"=>"686"
          ,"ext"=>".sn"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"152"
          ,"name"=>"Serbia"
          ,"currency"=>"Dinar"
          ,"code"=>"381"
          ,"iso"=>"RS"
          ,"iso2"=>"SRB"
          ,"c"=>"688"
          ,"ext"=>".rs and .yu"
          ,"new-currency"=>"RSD"
          
          )
          ,array(

          "id"=>"153"
          ,"name"=>"Seychelles"
          ,"currency"=>"Rupee"
          ,"code"=>"248"
          ,"iso"=>"SC"
          ,"iso2"=>"SYC"
          ,"c"=>"690"
          ,"ext"=>".sc"
          ,"new-currency"=>"SCR"
          
          )
          ,array(

          "id"=>"154"
          ,"name"=>"Sierra Leone"
          ,"currency"=>"Leone"
          ,"code"=>"232"
          ,"iso"=>"SL"
          ,"iso2"=>"SLE"
          ,"c"=>"694"
          ,"ext"=>".sl"
          ,"new-currency"=>"SLL"
          
          )
          ,array(

          "id"=>"155"
          ,"name"=>"Singapore"
          ,"currency"=>"Dollar"
          ,"code"=>"65"
          ,"iso"=>"SG"
          ,"iso2"=>"SGP"
          ,"c"=>"702"
          ,"ext"=>".sg"
          ,"new-currency"=>"SGD"
          
          )
          ,array(

          "id"=>"156"
          ,"name"=>"Slovakia"
          ,"currency"=>"Koruna"
          ,"code"=>"421"
          ,"iso"=>"SK"
          ,"iso2"=>"SVK"
          ,"c"=>"703"
          ,"ext"=>".sk"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"157"
          ,"name"=>"Slovenia"
          ,"currency"=>"Euro"
          ,"code"=>"386"
          ,"iso"=>"SI"
          ,"iso2"=>"SVN"
          ,"c"=>"705"
          ,"ext"=>".si"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"158"
          ,"name"=>"Solomon Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"677"
          ,"iso"=>"SB"
          ,"iso2"=>"SLB"
          ,"c"=>"90"
          ,"ext"=>".sb"
          ,"new-currency"=>"SBD"
          
          )
          ,array(

          "id"=>"159"
          ,"name"=>"Somalia"
          ,"currency"=>"Shilling"
          ,"code"=>"252"
          ,"iso"=>"SO"
          ,"iso2"=>"SOM"
          ,"c"=>"706"
          ,"ext"=>".so"
          ,"new-currency"=>"SOS"
          
          )
          ,array(

          "id"=>"199"
          ,"name"=>"Somaliland"
          ,"currency"=>"Shilling"
          ,"code"=>"252"
          ,"iso"=>"SO"
          ,"iso2"=>"SOM"
          ,"c"=>"706"
          ,"ext"=>".so"
          ,"new-currency"=>"SOS"
          
          )
          ,array(

          "id"=>"160"
          ,"name"=>"South Africa"
          ,"currency"=>"Rand"
          ,"code"=>"27"
          ,"iso"=>"ZA"
          ,"iso2"=>"ZAF"
          ,"c"=>"710"
          ,"ext"=>".za"
          ,"new-currency"=>"ZAR"
          
          )
          ,array(

          "id"=>"200"
          ,"name"=>"South Ossetia"
          ,"currency"=>"Ruble and Lari"
          ,"code"=>"995"
          ,"iso"=>"GE"
          ,"iso2"=>"GEO"
          ,"c"=>"268"
          ,"ext"=>".ge"
          ,"new-currency"=>"RUB"
          
          )
          ,array(

          "id"=>"161"
          ,"name"=>"Spain"
          ,"currency"=>"Euro"
          ,"code"=>"34"
          ,"iso"=>"ES"
          ,"iso2"=>"ESP"
          ,"c"=>"724"
          ,"ext"=>".es"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"162"
          ,"name"=>"Sri Lanka"
          ,"currency"=>"Rupee"
          ,"code"=>"94"
          ,"iso"=>"LK"
          ,"iso2"=>"LKA"
          ,"c"=>"144"
          ,"ext"=>".lk"
          ,"new-currency"=>"LKR"
          
          )
          ,array(

          "id"=>"163"
          ,"name"=>"Sudan"
          ,"currency"=>"Dinar"
          ,"code"=>"249"
          ,"iso"=>"SD"
          ,"iso2"=>"SDN"
          ,"c"=>"736"
          ,"ext"=>".sd"
          ,"new-currency"=>"SDG"
          
          )
          ,array(

          "id"=>"164"
          ,"name"=>"Suriname"
          ,"currency"=>"Dollar"
          ,"code"=>"597"
          ,"iso"=>"SR"
          ,"iso2"=>"SUR"
          ,"c"=>"740"
          ,"ext"=>".sr"
          ,"new-currency"=>"SRD"
          
          )
          ,array(

          "id"=>"261"
          ,"name"=>"Svalbard"
          ,"currency"=>"Krone"
          ,"code"=>"47"
          ,"iso"=>"SJ"
          ,"iso2"=>"SJM"
          ,"c"=>"744"
          ,"ext"=>".sj"
          ,"new-currency"=>"NOK"
          
          )
          ,array(

          "id"=>"165"
          ,"name"=>"Swaziland"
          ,"currency"=>"Lilangeni"
          ,"code"=>"268"
          ,"iso"=>"SZ"
          ,"iso2"=>"SWZ"
          ,"c"=>"748"
          ,"ext"=>".sz"
          ,"new-currency"=>"SZL"
          
          )
          ,array(

          "id"=>"166"
          ,"name"=>"Sweden"
          ,"currency"=>"Kronoa"
          ,"code"=>"46"
          ,"iso"=>"SE"
          ,"iso2"=>"SWE"
          ,"c"=>"752"
          ,"ext"=>".se"
          ,"new-currency"=>"SEK"
          
          )
          ,array(

          "id"=>"167"
          ,"name"=>"Switzerland"
          ,"currency"=>"Franc"
          ,"code"=>"41"
          ,"iso"=>"CH"
          ,"iso2"=>"CHE"
          ,"c"=>"756"
          ,"ext"=>".ch"
          ,"new-currency"=>"CHF"
          
          )
          ,array(

          "id"=>"168"
          ,"name"=>"Syria"
          ,"currency"=>"Pound"
          ,"code"=>"963"
          ,"iso"=>"SY"
          ,"iso2"=>"SYR"
          ,"c"=>"760"
          ,"ext"=>".sy"
          ,"new-currency"=>"SYP"
          
          )
          ,array(

          "id"=>"169"
          ,"name"=>"Tajikistan"
          ,"currency"=>"Somoni"
          ,"code"=>"992"
          ,"iso"=>"TJ"
          ,"iso2"=>"TJK"
          ,"c"=>"762"
          ,"ext"=>".tj"
          ,"new-currency"=>"TJS"
          
          )
          ,array(

          "id"=>"170"
          ,"name"=>"Tanzania"
          ,"currency"=>"Shilling"
          ,"code"=>"255"
          ,"iso"=>"TZ"
          ,"iso2"=>"TZA"
          ,"c"=>"834"
          ,"ext"=>".tz"
          ,"new-currency"=>"TZS"
          
          )
          ,array(

          "id"=>"171"
          ,"name"=>"Thailand"
          ,"currency"=>"Baht"
          ,"code"=>"66"
          ,"iso"=>"TH"
          ,"iso2"=>"THA"
          ,"c"=>"764"
          ,"ext"=>".th"
          ,"new-currency"=>"THB"
          
          )
          ,array(

          "id"=>"172"
          ,"name"=>"Timor-Leste (East Timor)"
          ,"currency"=>"Dollar"
          ,"code"=>"670"
          ,"iso"=>"TL"
          ,"iso2"=>"TLS"
          ,"c"=>"626"
          ,"ext"=>".tp and .tl"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"173"
          ,"name"=>"Togo"
          ,"currency"=>"Franc"
          ,"code"=>"228"
          ,"iso"=>"TG"
          ,"iso2"=>"TGO"
          ,"c"=>"768"
          ,"ext"=>".tg"
          ,"new-currency"=>"XOF"
          
          )
          ,array(

          "id"=>"219"
          ,"name"=>"Tokelau"
          ,"currency"=>"Dollar"
          ,"code"=>"690"
          ,"iso"=>"TK"
          ,"iso2"=>"TKL"
          ,"c"=>"772"
          ,"ext"=>".tk"
          ,"new-currency"=>"NZD"
          
          )
          ,array(

          "id"=>"174"
          ,"name"=>"Tonga"
          ,"currency"=>"Paanga"
          ,"code"=>"676"
          ,"iso"=>"TO"
          ,"iso2"=>"TON"
          ,"c"=>"776"
          ,"ext"=>".to"
          ,"new-currency"=>"TOP"
          
          )
          ,array(

          "id"=>"175"
          ,"name"=>"Trinidad and Tobago"
          ,"currency"=>"Dollar"
          ,"code"=>"-867"
          ,"iso"=>"TT"
          ,"iso2"=>"TTO"
          ,"c"=>"780"
          ,"ext"=>".tt"
          ,"new-currency"=>"TTD"
          
          )
          ,array(

          "id"=>"176"
          ,"name"=>"Tunisia"
          ,"currency"=>"Dinar"
          ,"code"=>"216"
          ,"iso"=>"TN"
          ,"iso2"=>"TUN"
          ,"c"=>"788"
          ,"ext"=>".tn"
          ,"new-currency"=>"TND"
          
          )
          ,array(

          "id"=>"177"
          ,"name"=>"Turkey"
          ,"currency"=>"Lira"
          ,"code"=>"90"
          ,"iso"=>"TR"
          ,"iso2"=>"TUR"
          ,"c"=>"792"
          ,"ext"=>".tr"
          ,"new-currency"=>"TRY"
          
          )
          ,array(

          "id"=>"178"
          ,"name"=>"Turkmenistan"
          ,"currency"=>"Manat"
          ,"code"=>"993"
          ,"iso"=>"TM"
          ,"iso2"=>"TKM"
          ,"c"=>"795"
          ,"ext"=>".tm"
          ,"new-currency"=>"TMT"
          
          )
          ,array(

          "id"=>"235"
          ,"name"=>"Turks and Caicos Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"-648"
          ,"iso"=>"TC"
          ,"iso2"=>"TCA"
          ,"c"=>"796"
          ,"ext"=>".tc"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"179"
          ,"name"=>"Tuvalu"
          ,"currency"=>"Dollar"
          ,"code"=>"688"
          ,"iso"=>"TV"
          ,"iso2"=>"TUV"
          ,"c"=>"798"
          ,"ext"=>".tv"
          ,"new-currency"=>"AUD"
          
          )
          ,array(

          "id"=>"248"
          ,"name"=>"U.S. Virgin Islands"
          ,"currency"=>"Dollar"
          ,"code"=>"-339"
          ,"iso"=>"VI"
          ,"iso2"=>"VIR"
          ,"c"=>"850"
          ,"ext"=>".vi"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"180"
          ,"name"=>"Uganda"
          ,"currency"=>"Shilling"
          ,"code"=>"256"
          ,"iso"=>"UG"
          ,"iso2"=>"UGA"
          ,"c"=>"800"
          ,"ext"=>".ug"
          ,"new-currency"=>"UGX"
          
          )
          ,array(

          "id"=>"181"
          ,"name"=>"Ukraine"
          ,"currency"=>"Hryvnia"
          ,"code"=>"380"
          ,"iso"=>"UA"
          ,"iso2"=>"UKR"
          ,"c"=>"804"
          ,"ext"=>".ua"
          ,"new-currency"=>"UAH"
          
          )
          ,array(

          "id"=>"182"
          ,"name"=>"United Arab Emirates"
          ,"currency"=>"Dirham"
          ,"code"=>"971"
          ,"iso"=>"AE"
          ,"iso2"=>"ARE"
          ,"c"=>"784"
          ,"ext"=>".ae"
          ,"new-currency"=>"AED"
          
          )
          ,array(

          "id"=>"183"
          ,"name"=>"United Kingdom"
          ,"currency"=>"Pound"
          ,"code"=>"44"
          ,"iso"=>"GB"
          ,"iso2"=>"GBR"
          ,"c"=>"826"
          ,"ext"=>".uk"
          ,"new-currency"=>"GBP"
          
          )
          ,array(

          "id"=>"184"
          ,"name"=>"United States"
          ,"currency"=>"Dollar"
          ,"code"=>"1"
          ,"iso"=>"US"
          ,"iso2"=>"USA"
          ,"c"=>"840"
          ,"ext"=>".us"
          ,"new-currency"=>"USD"
          
          )
          ,array(

          "id"=>"185"
          ,"name"=>"Uruguay"
          ,"currency"=>"Peso"
          ,"code"=>"598"
          ,"iso"=>"UY"
          ,"iso2"=>"URY"
          ,"c"=>"858"
          ,"ext"=>".uy"
          ,"new-currency"=>"UYU"
          
          )
          ,array(

          "id"=>"186"
          ,"name"=>"Uzbekistan"
          ,"currency"=>"Som"
          ,"code"=>"998"
          ,"iso"=>"UZ"
          ,"iso2"=>"UZB"
          ,"c"=>"860"
          ,"ext"=>".uz"
          ,"new-currency"=>"UZS"
          
          )
          ,array(

          "id"=>"187"
          ,"name"=>"Vanuatu"
          ,"currency"=>"Vatu"
          ,"code"=>"678"
          ,"iso"=>"VU"
          ,"iso2"=>"VUT"
          ,"c"=>"548"
          ,"ext"=>".vu"
          ,"new-currency"=>"VUV"
          
          )
          ,array(

          "id"=>"188"
          ,"name"=>"Vatican City"
          ,"currency"=>"Euro"
          ,"code"=>"379"
          ,"iso"=>"VA"
          ,"iso2"=>"VAT"
          ,"c"=>"336"
          ,"ext"=>".va"
          ,"new-currency"=>"EUR"
          
          )
          ,array(

          "id"=>"189"
          ,"name"=>"Venezuela"
          ,"currency"=>"Bolivar"
          ,"code"=>"58"
          ,"iso"=>"VE"
          ,"iso2"=>"VEN"
          ,"c"=>"862"
          ,"ext"=>".ve"
          ,"new-currency"=>"VEF"
          
          )
          ,array(

          "id"=>"190"
          ,"name"=>"Vietnam"
          ,"currency"=>"Dong"
          ,"code"=>"84"
          ,"iso"=>"VN"
          ,"iso2"=>"VNM"
          ,"c"=>"704"
          ,"ext"=>".vn"
          ,"new-currency"=>"VND"
          
          )
          ,array(

          "id"=>"213"
          ,"name"=>"Wallis and Futuna"
          ,"currency"=>"Franc"
          ,"code"=>"681"
          ,"iso"=>"WF"
          ,"iso2"=>"WLF"
          ,"c"=>"876"
          ,"ext"=>".wf"
          ,"new-currency"=>"CFP"
          
          )
          ,array(

          "id"=>"191"
          ,"name"=>"Yemen"
          ,"currency"=>"Rial"
          ,"code"=>"967"
          ,"iso"=>"YE"
          ,"iso2"=>"YEM"
          ,"c"=>"887"
          ,"ext"=>".ye"
          ,"new-currency"=>"YER"
          
          )
          ,array(

          "id"=>"192"
          ,"name"=>"Zambia"
          ,"currency"=>"Kwacha"
          ,"code"=>"260"
          ,"iso"=>"ZM"
          ,"iso2"=>"ZMB"
          ,"c"=>"894"
          ,"ext"=>".zm"
          ,"new-currency"=>"ZMW"
          
          )
          ,array(

          "id"=>"193"
          ,"name"=>"Zimbabwe"
          ,"currency"=>"Dollar"
          ,"code"=>"263"
          ,"iso"=>"ZW"
          ,"iso2"=>"ZWE"
          ,"c"=>"716"
          ,"ext"=>".zw"
          ,"new-currency"=>"ZWD"
          
          )

    );
}
