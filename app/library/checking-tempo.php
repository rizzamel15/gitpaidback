<?php

class checking_tempo //CHECKIN CLASS ONLY
{
	public static function _check_null_params($array_params,$bool_only = null)
	{
		$collection_array = $array_params['collection-array'];
		
		$empty_list = array();
		$is_empty_true = false;
		
		foreach($collection_array as $key=>$val)
		{
			if($val=='' || is_null($val))
			{
				array_push($empty_list,$val);
				$is_empty_true = true;
			}
		}
		
		if($bool_only!=null){return $is_empty_true;} //BOOLEAN ONLY
		
		return array('empty-list'=>$empty_list,'is-empty-true'=>$is_empty_true);
	}
	public static function _check_missing_params($array_params,$bool_only = null)
	{
		$required_array = $array_params['required-array'];
		$collection_array = $array_params['collection-array'];
		
		$is_missing_true = false;
		$missing_list = array();

		foreach($required_array as $key=>$val)
		{
			if(!array_key_exists($val,$collection_array))
			{
				$is_missing_true = true;
				array_push($missing_list,$val);
			}
		}
		
		if($bool_only!=null){return $is_missing_true;} //BOOLEAN ONLY
		
		return array('missing-list'=>$missing_list,'is-missing-true'=>$is_missing_true);
	}
	
}