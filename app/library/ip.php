<?php

class ip
{
   protected static $default_country = "PH";
   protected static $server_url = "https://freegeoip.net/json";
   protected static $default_location = array(  "country"=>"Philippines",
                                                "currency"=>"PHP",
                                                "time_zone"=>"Asia/Manila");
   protected static $sess_loc_name = "sess-location"; 
   private static function _get_real_ip()
   {
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = $_SERVER['REMOTE_ADDR'];
           
      if(filter_var($client, FILTER_VALIDATE_IP)){
         $ip = $client;
      }
      elseif(filter_var($forward, FILTER_VALIDATE_IP)){
         $ip = $forward;
      }
      else{
         $ip = $remote;
      }
           
      return $ip;
   }
   public static  function _get_ip()
   {
      $host = $_SERVER['HTTP_HOST'];
      $ip = $_SERVER['REMOTE_ADDR'];
      
      if($host=='localhost'){
         $ip = '';
      }
      else{
         $ip = self::_get_real_ip();
      }
      
      return $ip;
   }  
   private static function _request()
   {
      $response = @file_get_contents(self::$server_url.'/'.self::_get_ip(),true);
      
      if($response==false){$response = '';}
            
      return $response;
   }
   private  static function _locate_address()
   {
           
      $country_code = p_code::_get_country_code();
      $location_array = self::$default_location;
            
      $response_str = self::_request();
      
      if($response_str!='')
      {
         $response_array = json_decode($response_str,true);
         /*{"ip":"124.6.181.84","country_code":"PH",
         "country_name":"Philippines","region_code":"40",
         "region_name":"Calabarzon","city":"Batangas",
         "zip_code":"4200",
         "time_zone":"Asia/Manila",
         "latitude":13.757,"longitude":121.058,
         "metro_code":0}*/
                
         if(isset($response_array['country_name']))
         {
            $country_name = $response_array['country_name'];
            $timezone = $response_array['time_zone'];
            
            for($x=0;$x<count($country_code);$x++)
            {
               if(strtolower($country_code[$x]['name'])==strtolower($country_name))
               {
                  $location_array = array(
                              "country"=>$country_name,
                              "currency"=>$country_code[$x]['new-currency'],
                              "time_zone"=>$timezone);
                  break 1;
               }        
            }
         } 
      }
		
      self::_set_loc($location_array);
   }
   public static function _set_loc($location_array)
   {
      $_SESSION[self::$sess_loc_name] = $location_array;
   }
   public static function _get_loc()
   {	
      $location_array = self::$default_country;

      if(isset($_SESSION[self::$sess_loc_name])){
         $location_array = $_SESSION[self::$sess_loc_name];
      }
      else{
         self::_locate_address();
         $location_array = self::_get_loc();
      }
      
      return $location_array;
   }
   public static function _get_iso_flags($country)
   {
	    
      $country_code = p_code::_get_country_code();
      $iso='';

      for($x=0;$x<count($country_code);$x++)
      {
         if(strtolower($country)==strtolower($country_code[$x]['name']))
         {
            $iso = strtolower($country_code[$x]['iso']);
            break 1;
         }
      }
	    
      return 'flag flag-'.$iso;
   }     
}