<?php

class api
{
	//var $required_key = array('descriptor','cardtype','bank','purchase','date-of-purchase','email-address','refund-type','first6','last4');
	var $required_key = array('descriptor','purchase','date-of-purchase','email-address','first6','last4','purchase-name');

	
																				//API REFUND
	//************************************************************************				************************************************************************
	//************************************************************************				************************************************************************
	//************************************************************************				************************************************************************
	/*_api/api_index.php?type=mobile&content={"descriptor":"SKYTRACE.COM%20662-705-6066%20FL%2033140","cardtype":"VISA","bank":"BOA","purchase":"5.00","date-of-purchase":"04/24/2014","first6":"433333","last4":"3333","refund-type":"REG","email-address":"marcelo.oblan@gmail.com"}*/					
	//************************************************************************				************************************************************************
	//************************************************************************				************************************************************************
	//************************************************************************				************************************************************************
	
	public function _index($array_params)
	{
		
		
		$output_class = new output;
		$checking_class = new checking;
		
		$type = $array_params['type'];
		$content_array = $array_params['content-array'];
		$content_array_key = array();
		foreach($content_array as $key=>$val){array_push($content_array_key,$key);}
		
		$required_key = $this->required_key;
		
		$missing_bool = 0;
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		//						
		
		//CHECKING EMPTY FIELD AND MSSING KEY
		$missing_bool = $checking_class->_missing_var(array('content-array-key'=>$content_array_key,'content-array'=>$content_array,'required-key'=>$required_key));
		
		
		if($missing_bool==1){$output_class->json(array('type'=>'failed','sub-type'=>'missing','data'=>'','content-array'=>$content_array,'result-array'=>array()));}
		elseif($missing_bool==2){$output_class->json(array('type'=>'failed','sub-type'=>'empty','data'=>'','content-array'=>$content_array,'result-array'=>array()));}
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		
			
		
		$content_array = $checking_class->_sanitize(array('array-data'=>$content_array));									//CLEANING FOR BAD CHARACTER
		

		//CHECK IF AMOUNT VALID
		$checking_class->_is_amount_valid(array('content-array'=>$content_array,'result-array'=>array()));
		$checking_class->_is_card_valid(array('content-array'=>$content_array,'result-array'=>array()));																													//END CHECK AMOUNT VALID
				
//error_log(print_r($content_array,true),0);																										//START TRANSACTION PROCESS
		$transaction_class = new transaction;																														
		$transaction_class->_start(array('content-array'=>$content_array));		

//END TRANSACTION PROCESS																																																									
	}
	
	
}