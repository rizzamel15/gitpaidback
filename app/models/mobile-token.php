<?php
ob_start();
error_reporting(0);

class Mobile_Token extends Model
{
	public function _verify_token($array_params)
	{
		$pre_login_id = 0;

		$query_data = $this->db->select("SELECT pre_login_id FROM tempic4c.pre_login WHERE mobile_token = :mobile_token", array(":mobile_token" => $array_params));
		
		if(count($query_data)==0){
			SharedResponse::requirements_response_halt(2);
		}

		$pre_login_id = $query_data[0]['pre_login_id'];
		return $pre_login_id;
	}
	
	public function _create_token($array_params)
	{
		$timestamp = date('Ymdhis');
		$token = sha1($array_params.$timestamp);

		$post_data = array('mobile_token' => $token);
		$this->db->update("tempic4c.pre_login", $post_data, "`PRE_LOGIN_ID` = {$array_params}");
		return $token;
	}
	
	public function _change_token($pre_login_id)
	{
		$timestamp = date('Ymdhis');
		$token = sha1($pre_login_id.$timestamp);
		$post_data = array('mobile_token' => $token);
		$this->db->update("tempic4c.pre_login", $post_data, "`PRE_LOGIN_ID` = {$pre_login_id}");

		return $token;
	}
}

ob_end_flush();