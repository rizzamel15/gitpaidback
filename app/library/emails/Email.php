<?php

class Email
{
	public static function auth_mail($array_params,$from_name=null)
	{
		$to = $array_params[0];
		$subject = $array_params[1];
		$message = $array_params[2];
		$from = $array_params[3];

		if($from_name==null){
			$from_name = $from;
		}

		$mail_string='https://www.ican4consumers.com/mail/mail_api.php';

		$post_string='apikey=9d3bf9fe2ad81118bc5710274f962a16bee26722&from-email='.$from.'&from-name='.$from_name.'&subject='.$subject.'&to-email='.$to;

		$post_string.='&message='.rawurlencode($message).'&html=true'; 
		
	    $return_response = json_decode(self::_curl($mail_string,$post_string),true);
		
	    if(isset($return_response['responsecode']))
	    {
	    	if($return_response['responsecode']==-1){
				mail('drizzamel15@gmail.com','debugging mailer alert',print_r($return_response,true).': '.print_r($mail_string.'?'.$post_string,true));
	    	}
	    }
	    return $return_response;
	}
	private static function _curl($url,$post)
	{
		$value='';
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_HEADER,0);
		curl_setopt($curl,CURLOPT_POST,1);
		curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,60);
		curl_setopt($curl,CURLOPT_TIMEOUT,60);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt($curl,CURLOPT_VERBOSE,true);
		$value = curl_exec($curl);
		$value.=curl_error($curl);
		curl_close($curl);
		return $value;
	}
	public static function response($mail_response, $message, $text)
	{
		if($mail_response['responsecode']==1)
		{
    		$array_response = array('responsecode'=>1,'responsestring'=>$text);
    		Response::json_output($array_response);
		}else{
    		mail('drizzamel15@gmail.com','debugging mailer alert',print_r($mail_response,true).': '.print_r($message,true));
		}
	}
}