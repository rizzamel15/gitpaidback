<?php

class Main_Curl
{
	public static function _curl($url,$post)
	{
		$value='';
			
		$curl = curl_init();
		curl_setopt($curl,CURLOPT_URL,$url);
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($curl,CURLOPT_HEADER,0);
		curl_setopt($curl,CURLOPT_POST,1);
		curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,10);
		curl_setopt($curl,CURLOPT_TIMEOUT,10);
		curl_setopt($curl,CURLOPT_POSTFIELDS,$post);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt($curl,CURLOPT_VERBOSE,true);
		$value = curl_exec($curl);
		$value.=curl_error($curl);
		curl_close($curl);
		//return curl_getinfo($curl);
		return $value;
	}
}